#!/usr/bin/python3
# -*- coding: utf-8 -*-

#import Nime as jeu
import Nime as jeu
import random
import math



def min_max_alphabeta(situation, profondeur, joueur,alpha,beta):
    '''
    Détermine une évaluation numérique de la situation donnée en paramètre de manière
    récursive.
    
    :param:
    situation : (list) (list) Grille2d représentant le jeu. Liste de liste contenant
    des 0 (vide), 1 (joueur 1) ou 2 (joueur 2).
    
    :renvoie:
    (int)
    
    :effet de bord:
    Aucun
    '''
    PROGRAMME = 2
    ADVERSAIRE = 1
    if jeu.est_fini(situation) or profondeur ==0:
        coeff = 1 - 2 * (joueur ==ADVERSAIRE) # 1 si PROGRAMME, -1 si ADVERSAIRE
        return jeu.fonct_evaluation(situation, joueur) * coeff
    else:
        cps_possi = jeu.coups_possibles(situation, joueur)
        situ_suivantes = [jeu.mise_a_jour_plateau(coup_joue, joueur, situation)
                          for coup_joue in cps_possi]
        if joueur ==PROGRAMME:
            for situ_fille in situ_suivantes:
                valeur = max(alpha, min_max_alphabeta(situ_fille, profondeur - 1,
                                                      ADVERSAIRE,alpha,beta))
                alpha = max(alpha,valeur)
                if beta <= alpha:
                    return valeur
                
        else: # joueur ==ADVERSAIRE
            for situ_fille in situ_suivantes:
                valeur = min(beta, min_max_alphabeta(situ_fille, profondeur - 1,
                                                        PROGRAMME,alpha,beta))
                beta = min(beta, valeur)
                if beta <= alpha:
                    return valeur
        return valeur
    return 

def coup_choisi_IA(situation, liste_coups ,k):
    '''
    Détermine par l'algorithme min-max, quelle situation fille de la situation
    donnée en paramètre permet d'obtenir la situation, à priori, la plus
    favorable pour l'IA.
    La fonction renvoie le coup joué par l'IA pour obtenir cette situation fille.
    
    :param:
    situation : (list) Contient l'état du jeu avant que l'IA ne joue.
    liste_coups : (list) Liste des coups valides que peut jouer l'IA
    
    :return:
    (élément de liste_coups)
    '''
    PROGRAMME = 2
    ADVERSAIRE = 1
    PROFONDEUR = k
    situ_suivantes = [jeu.mise_a_jour_plateau(coup_joue, PROGRAMME, situation)
                      for coup_joue in liste_coups] # Etude des possibilités de jeu de l'ordinateur
    coups_possi_pondere = [min_max_alphabeta(situation_fille,PROFONDEUR, ADVERSAIRE,
                                              -math.inf, +math.inf)
                           for situation_fille in situ_suivantes] # Pour chaque situation fille, calcul de la pondération de cette situation.
    poids_max = max(coups_possi_pondere) # Parmi les situations filles, quelle est la pndération maximale ?
    num_coup_choisi = coups_possi_pondere.index(poids_max) # Parmi les situations filles, quelle est l'indice de celle qui a la pondération maximale ?
    return liste_coups[num_coup_choisi]

def joueur_vs_joueur(liste_coups, joueur_courant = None):
    return jeu.coup_choisi(liste_coups)

def joueur_vs_IAidiot(liste_coups, joueur_courant):
    JOUEUR1 = 1
    if joueur_courant ==JOUEUR1:
        rep = jeu.coup_choisi(liste_coups)
    else:
        rep = random.choice(liste_coups)
    return rep

def joueur_vs_IA(situation, liste_coups, joueur_courant, k):
    JOUEUR1 = 1
    if joueur_courant ==JOUEUR1:
        rep = jeu.coup_choisi(liste_coups)
    else:
        rep = coup_choisi_IA(situation, liste_coups, k)
    return rep    
 
def jouer(adversaire = 2, k = 26):
    plateau = jeu.installation_jeu ()
    jeu.affiche_plateau(plateau)
    joueur_courant = jeu.premier_joueur ()
    while  not jeu.est_fini (plateau):
        liste_coups = jeu.coups_possibles (plateau, joueur_courant)
        jeu.affiche_coups(liste_coups,joueur_courant)
        if jeu.peut_jouer (liste_coups):
    
            if adversaire ==0:
                coup_joue = joueur_vs_joueur (liste_coups, joueur_courant)
            elif adversaire ==1:
                coup_joue = joueur_vs_IAidiot (liste_coups, joueur_courant)
            else:
                coup_joue = joueur_vs_IA (plateau, liste_coups, joueur_courant, k)
    
            plateau = jeu.mise_a_jour_plateau (coup_joue, joueur_courant, plateau)
            jeu.affiche_plateau(plateau)
        joueur_courant = jeu.changement_de_joueur (joueur_courant)
    joueur_gagnant = jeu.resultat_jeu (plateau,joueur_courant)
    jeu.afficher_gagnant (joueur_gagnant)
    
jouer()
