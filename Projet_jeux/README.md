# Eléments du projet
## _jeu à deux joueurs, algorithme min-Max_
### *Marie Guichard, Sébastien Sauvage*
* **Interface** des jeux au format Python :
    * Jouer 1 vs 1 : [jouer.py](jouer.py)
    * Jouer 1 vs 1 ou 1 vs IA : [jouer_vs_IA.py](jouer_vs_IA.py) (A faire : *codage de la fonction du choix de l'ordinateur*)
    * Jouer 1 vs IA amélioré (alpha-béta) : [jouer_vs_IA_alphabeta.py]() *à venir ...*
* Modules **nim** au format Python :
	* humain vs humain : [Nime.py](Nime.py)
	* humain vs IA : []() (*A faire : la fonction d'évaluation*)
* Modules **puissance 4** au format Python :
	* humain vs humain : [puissance_4_v1.py](puissance_4_v1.py)
	* humain vs IA : [puissance_4_v2.py](puissance_4_v2.py) (*A faire : la fonction d'évaluation*)
* Sujet élève pour la mise en oeuvre de puissance 4 :
	* format md : [fiche eleve puissance 4](./fiche eleve puissance 4/puissance_4_eleve.md)