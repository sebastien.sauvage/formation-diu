#!/usr/bin/python3
# -*- coding: utf-8 -*-
import timeit
import pylab

###################
#Première partie
#

def mesure_tps(n):
    '''
    Renvoie la mesure de temps pour trier des listes de cette longueur avec le tri selection.

    :param:
    n : (int) longueur de la liste à trier

    :return:
    (float)
    '''
    tps =  timeit.timeit(
        setup='from tris import tri_select; from listes import cree_liste_melangee',
           stmt = 'tri_select(cree_liste_melangee('+str(n)+'))',
           number = 100
           )
    return tps

def mesure_tps_max(nmax):
    '''
    Renvoie les mesures de temps pour des listes de longueur inférieur au paramètre.

    :param:
    n : (int) longueur maximale de la liste à trier

    :return:
    (list)
    '''
    return list(mesure_tps(i) for i in range(nmax))

def courbe_tps(nmax):
    '''
    Tracer la courbe du temps en fonction de la longueur des listes triées par
    l'algorithme de tri par sélection

    :param:
    nmax : (int) taille maximale des listes étudiées.

    :renvoie:
    (None Type)

    :Effet de bord:
    Trace une courbe du temps d'execution en fonction de la longueur de la liste triée
    '''
    x = list (i + 1 for i in range(nmax))
    y = mesure_tps_max(nmax)
    pylab.plot(x, y)
    NBRE_ESSAIS = 100
    pylab.title('Temps du tri par sélection (pour {:d} essais)'.format(NBRE_ESSAIS))
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    pylab.show()
    return

###################
#Deuxième partie
#

def mesure_tps2(comp, n):
    '''
    Renvoie la mesure de temps pour trier des listes de cette longueur avec le tri comp.

    :param:
    comp : (str) type de tri à effectuer
    n : (int) longueur de la liste à trier

    :CU:
    comp appartient à [tri_select, tri_insert, tri_rapide, tri_fusion]

    :return:
    (float)
    '''
    tps =  timeit.timeit(
        setup='from tris import ' + comp + '; from listes import cree_liste_melangee',
           stmt = comp + '(cree_liste_melangee('+str(n)+'))',
           number = 100
           )
    '''    tps =  timeit.timeit(
        setup='from tris import tri_fusion; from listes import cree_liste_melangee',
           stmt = 'tri_fusion(cree_liste_melangee('+str(n)+'))',
           number = 100
           )'''
    return tps

def mesure_tps_max2(comp, nmax):
    '''
    Renvoie les mesures de temps pour des listes de longueur inférieur au paramètre.

    :param:
    comp : (str) type de tri à effectuer
    n : (int) longueur maximale de la liste à trier

    :CU:
    comp appartient à [tri_select, tri_insert, tri_rapide, tri_fusion]

    :return:
    (list)
    '''
    return list(mesure_tps2(comp, i) for i in range(nmax))




def mesure_tps3(comp, n):
    tps =  timeit.timeit(
        setup='from tris import ' + comp + '; from listes import cree_liste_croissante',
           stmt = comp + '(cree_liste_croissante('+str(n)+'))',
           number = 100
           )
    return tps

def mesure_tps_max3(comp, nmax):
    return list(mesure_tps3(comp, i) for i in range(nmax))





def mesure_tps_max4(comp, nmax):
    return list(mesure_tps4(comp, i) for i in range(nmax))

def mesure_tps4(comp, n):
    tps =  timeit.timeit(
        setup='from tris import ' + comp + '; from listes import cree_liste_decroissante',
           stmt = comp + '(cree_liste_decroissante('+str(n)+'))',
           number = 100
           )
    return tps





def courbe_tps2(comp, nmax):
    '''
    Tracer la courbe du temps en fonction de la longueur des listes triées par
    l'algorithme de tri par sélection

    :param:
    comp : (str) type de tri à effectuer
    nmax : (int) taille maximale des listes étudiées.

    :CU:
    comp appartient à [tri_select, tri_insert, tri_rapide, tri_fusion]

    :renvoie:
    (None Type)

    :Effet de bord:
    Trace une courbe du temps d'execution en fonction de la longueur de la liste triée
    '''
    x = list (i + 1 for i in range(nmax))


    '''


    y=[]
    liste_tris=['tri_select', 'tri_insert', 'tri_rapide', 'tri_fusion', 'tri_python']
    for i in range(len(liste_tris)):
        comp = liste_tris[i]
        print ("Analyse du tri : ", comp)
        y.append(mesure_tps_max4(comp, nmax))
    for i in range(len(y)):
        pylab.plot(x, y[i])
    pylab.title('Temps du tri (pour {:d} essais)'.format(nmax))

    '''

    y = mesure_tps_max3(comp, nmax)
    pylab.plot(x, y)
    pylab.title('Temps du tri ' + comp + ' (pour {:d} essais)'.format(nmax))



    #'''


    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    pylab.show()

    return

#for type_tri in ['tri_select', 'tri_insert', 'tri_rapide', 'tri_fusion', 'tri_python']:
#    courbe_tps2(type_tri, 100)


if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)

