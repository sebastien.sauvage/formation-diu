#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

:author: DIU-Bloc2 - Univ. Lille
:date: 2019, mai

Tris de listes

"""

import random

def compare_entier_croissant(a, b):
    """
    :param a: (int) un entier
    :param b: (int) un entier
    :return: (int)  
             * >0  si a est supérieur à b
             * 0 si a est égal à b
             * <0 si a est inférieur à b
    :CU: aucune
    :Exemples:

    >>> compare_entier_croissant(1, 3) < 0
    True
    """
    return a-b

def compare_entier_décroissant(a, b):
    """
    :param a: (int) un entier
    :param b: (int) un entier
    :return: (int) 
             * >0 si a est inférieur à b
             * 0 si a est égal à b
             * <0 si a est supérieur à b
    :CU: aucune
    :Exemples:

    >>> compare_entier_décroissant(1, 3) > 0
    True
    """
    return b-a
    
def est_trie(l, comp):
    """
    :param l: (type sequentiel) une séquence 
    :param comp: une fonction de comparaison
    :return: (bool) 
      - True si l est triée
      - False sinon
    :CU: les éléments de l doivent être comparables
    :Exemples:

    >>> est_trie([1, 2, 3, 4], compare_entier_croissant)
    True
    >>> est_trie([1, 2, 4, 3], compare_entier_croissant)
    False
    >>> est_trie([], compare_entier_croissant)
    True
    """
    i = 0
    res = True
    while res and i < len(l) - 1:
        res = comp(l[i], l[i+1]) <= 0
        i += 1
    return res

def cree_liste_melangee(n):
    """
    :param n: (int) Longueur de la liste à créer
    :return: une permutation des n premiers entiers
    :CU: n >= 0
    :Exemples:
        
    >>> l = cree_liste_melangee(5)
    >>> len(l)
    5
    >>> sorted(l) == [0, 1, 2, 3, 4]
    True
    """
    l = list(range(n))
    random.shuffle(l)
    return l


################################################
#                  TRI 1                       #
################################################


def tri_1(l, comp):
    """
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_1(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    """
    while not est_trie(l, comp):
        i = random.randint(0,len(l)-2)
        if comp(l[i], l[i+1]) > 0:
            tmp = l[i]
            l[i] = l[i+1]
            l[i+1] = tmp

################################################
#                   TRI 2                      #
################################################

def tri_insertion(l, comp):
    """
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_insertion(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    """
    n = len(l)
    for i in range(1, n):
        tmp = l[i]
        k = i
        while k >= 1 and comp(tmp, l[k - 1]) < 0 :
            l[k] = l[k - 1]
            k = k - 1
        l[k] = tmp


################################################
#                   TRI 3                      #
################################################

def fais_quelque_chose(l, comp, d, f):
    p = l[d]
    ip = d
    for i in range (d+1, f):
        if comp(p, l[i]) > 0:
            l[ip] = l[i]
            l[i] = l[ip+1]
            ip = ip + 1
    l[ip] = p
    return ip

def tri_rapide(l, comp, d=0, f=None):
    """
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :param d: un entier >= 0 et < len(l) (0 par défaut)
    :param f: un entier <= len(l) ou None. None équivaut à len(l)
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_rapide(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    """
    if f is None: f = len(l)
    if f - d > 1:
        ip = fais_quelque_chose(l, comp, d, f)
        tri_rapide(l, comp, d=d, f=ip)
        tri_rapide(l, comp, d=ip+1, f=f)

def tri_selection(l, comp):
    '''
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
   
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_selection(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    '''
    for nb in range (len(l), 0, -1):
        rg = 0
        for i in range (1, nb):
            if comp(l[i], l[rg]) <0:
                rg = i
        l.append(l[rg])
        del l[rg]

def compare_chaine_lexicographique(c1, c2):
    """
    :param c1: (str) une chaine
    :param c2: (str) une chaine
    :return: (int) 
             * <0 si c1 est avant à c2
             * 0 si c1 est égal à c2
             * >0 si C1 est après à c2
    :CU: aucune
    :Exemples:

    >>> compare_chaine_lexicographique("a", "b") < 0
    True
    >>> compare_chaine_lexicographique("u", "d") > 0
    True
    >>> compare_chaine_lexicographique("aseddr", "aseddr") == 0
    True
    >>> compare_chaine_lexicographique("aseddr", "aseddru") < 0
    True
    >>> compare_chaine_lexicographique("aseddr", "rrfttg") < 0
    True
    >>> compare_chaine_lexicographique("aseddr", "rrft") < 0
    True
    >>> compare_chaine_lexicographique("aser", "rrfttg") < 0
    True
    >>> compare_chaine_lexicographique("", "rrfttg") < 0
    True
    >>> compare_chaine_lexicographique("aseddr", "asettg") < 0
    True
    >>> compare_chaine_lexicographique("eetf", "asettg") > 0
    True
    """
    rep = 0
    if c1 =="":
        rep = -1
    elif c2 =="":
        rep = 1
    long = min(len(c1), len(c2))
    indice = 0
    while (indice <long) and (rep ==0):
        if c1[indice] <c2[indice]:
            rep = -1
        elif c1[indice] >c2[indice]:
            rep = 1
        indice += 1
    if rep ==0 and c1 !=c2:
        if len(c1) <len(c2):
            rep = -1
        else:
            rep = 1
    return rep

def compare_chaine_longueur(c1, c2):
    """
    :param c1: (str) une chaine
    :param c2: (str) une chaine
    :return: (int) 
             * <0 si len(c1) est avant à len(c2)
             * <0 si len(c1) == len(c2) et c1 avant c2
             * 0 si c1 == c2
             * >0 si len(c1) > len(c2)
             * >0 si len(c1) == len(c2) et si c1 est après à c2
    :CU: aucune
    :Exemples:

    >>> compare_chaine_longueur("a", "b") < 0
    True
    >>> compare_chaine_longueur("u", "d") > 0
    True
    >>> compare_chaine_longueur("aseddr", "aseddr") == 0
    True
    >>> compare_chaine_longueur("aseddr", "aseddru") < 0
    True
    >>> compare_chaine_longueur("aseddr", "rrfttg") < 0
    True
    >>> compare_chaine_longueur("aseddr", "rrft") > 0
    True
    >>> compare_chaine_longueur("zaser", "rrfttg") < 0
    True
    >>> compare_chaine_longueur("", "rrfttg") < 0
    True
    >>> compare_chaine_longueur("aseddr", "asettg") < 0
    True
    >>> compare_chaine_longueur("eetf", "asettg") < 0
    True
    """
    if len(c2) == len(c1):
        rep = compare_chaine_lexicographique(c1, c2)
    else:
        rep = len(c1) - len(c2)
    return rep

def fusion(l1, l2, comp):
    '''
    :param l1: (list) une liste
    :param l2: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l1 et l2 doivent être comparables
    :Effet de bord: l1 et l2 sont des chaînes vides à la fin de la fonction
   
    :Exemples:

    >>> fusion ([1,3,4],[2,5,6],compare_entier_croissant) == [1, 2, 3, 4, 5, 6]
    True
    '''
    l = []
    while (len(l1) + len(l2)) !=0:
        if (len(l1) !=0) and (len(l2) !=0):
            if comp(l1[0], l2[0]) <0:
                l.append(l1.pop(0))
            else:
                l.append(l2.pop(0))
        else:
            if len(l1) >0:
                l = l + l1
                l1 = []
            else:
                l = l + l2
                l2 = []
    return l
            
def tri_fusion(l, comp):
    '''
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
   
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_fusion(l, compare_entier_croissant) == [1, 1, 2, 3, 4, 5, 9]
    True

    '''
    if len(l) >= 2:
        lg = l[: len(l) // 2]
        ld = l[len(l)//2:]
        lg = list(tri_fusion(lg, comp))
        ld = list(tri_fusion(ld, comp))
        l = list(fusion(lg, ld, comp))
    return l       


if __name__ == "__main__":
    import doctest
    doctest.testmod()
