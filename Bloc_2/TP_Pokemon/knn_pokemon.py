#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod:`knn_pokemon` module

:author: `FIL - Département Informatique - Université de Lille <http://portail.fil.univ-lille1.fr>`_

:date: Juillet, 2019

Module for knn algorithm.


"""

from pokemon import *

def comparaison_couples(couple1, couple2):
    '''
    Fonction de comparaison de deux couples selon la valeur du premier élément.
    Si "couple1 < couple2", renvoie -1,
    si "couple1 == couple2", renvoie 0,
    si "couple1 > couple2", renvoie 1.
    
    :param:
    couple1, couple2 : (élément indexable : list, tuple, str ...)
    
    :renvoie:
    (int)
    
    >>> comparaison_couples( (5, 'p'), (9, 'd') ) == -1
    True
    >>> comparaison_couples( (9, 'd'), (5, 'p') ) == 1
    True
    >>> comparaison_couples( (5, 'p'), (5, 'd') ) == 0
    True
    '''
    rep = 0
    if couple1[0] < couple2[0]:
        rep = -1
    elif couple1[0] > couple2[0]:
        rep = 1
    return rep
        
def tri_insertion(l, comp = comparaison_couples):
    """
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    """
    n = len(l)
    for i in range(1, n):
        tmp = l[i]
        k = i
        while k >= 1 and comp(tmp, l[k - 1]) < 0 :
            l[k] = l[k - 1]
            k = k - 1
        l[k] = tmp
        
    return l

def trier(liste_couples):
    '''
    Trie la liste de couples donnée en paramètre selon le premier élément de chaque couple.
    
    :param:
    liste_de_couples : (list) Couples de type (distance, pokemon).
    
    :renvoie:
    (list)
    
    >>> trier ([(2,'o'), (1,'P'), (3,'k'), (4,'e'), (6,'o'), (5,'m'), (7,'n')])
    [(1, 'P'), (2, 'o'), (3, 'k'), (4, 'e'), (5, 'm'), (6, 'o'), (7, 'n')]
    '''
    return tri_insertion(liste_couples, comparaison_couples)

def nearest_neighbors(pokemon, train_data, k,
                      mini_maxi_values,
                      distance = pokemon_manhattan_distance):
    """
    :param pokemon: (Pokemon) the pokemon to be classified
    :param train_data: (list of Pokemon) the train dataset
    :param k: (int) number of neighbors
    :param mini_maxi_values: (dict) dictionnary that contains for every numeric attribute the couple of minimum and maximum values
    :param distance: (function) distance function used
    :return: (list of Pokemon) list of k nearest pokemon

    :Exemple:

    >>> l = read_pokemon_csv()
    >>> train, test = split_pokemons(l)
    >>> m_m_values = min_max_values(train)
    >>> len(nearest_neighbors(test[0], train, 4, m_m_values))
    4
    """
    liste_couple_dist_poke = []
    for poke2 in train_data:
        dist = distance(pokemon, poke2, mini_maxi_values)
        liste_couple_dist_poke.append((dist, poke2))
    liste_couple_dist_poke = trier(liste_couple_dist_poke)
    return liste_couple_dist_poke[:k]

def knn2(pokemon, train_data, k,
        mini_maxi_values,
        distance = pokemon_manhattan_distance,
        prop = 'is_legendary'):
    """
    :param pokemon: (Pokemon) the pokemon to be classified
    :param train_data: (list of Pokemon) the train dataset
    :param k: (int) number of neighbors
    :param mini_maxi_values: (dict) dictionnary that contains for every numeric attribute the couple of minimum and maximum values
    :param distance: (function) distance function used
    :param prop: (str) name of the class to be predicted
    :return: (any) the class mostly present in pokemon neighborhood

    :Exemple:

    >>> l = read_pokemon_csv()
    >>> train, test = split_pokemons(l)
    >>> m_m_values = min_max_values(train)
    >>> knn(test[0], train, 3, m_m_values) in { True, False }
    True
    """
    indice_prop = POKE_PROP_NAMES.index(prop)
    est_prop = {True : 0, False : 0}
    # Il faudra créer un dict vide et généré les clés au fur et à mesure qu'on les rencontre.
    liste_k_plus_proche = nearest_neighbors(pokemon, train_data, k,
                                            mini_maxi_values, distance)
    for cpl_dist_poke in liste_k_plus_proche:
        poke2 = cpl_dist_poke [1]
        if poke2 [indice_prop]:
            est_prop [True] = est_prop [False] + 1
        else:
            est_prop [False] = est_prop [False] + 1
    if est_prop[True] > est_prop [False]:
        rep = True
    else:
        rep = False
    return rep

def knn(pokemon, train_data, k,
        mini_maxi_values,
        distance = pokemon_manhattan_distance,
        prop = 'is_legendary'):
    """
    :param pokemon: (Pokemon) the pokemon to be classified
    :param train_data: (list of Pokemon) the train dataset
    :param k: (int) number of neighbors
    :param mini_maxi_values: (dict) dictionnary that contains for every numeric attribute the couple of minimum and maximum values
    :param distance: (function) distance function used
    :param prop: (str) name of the class to be predicted
    :return: (any) the class mostly present in pokemon neighborhood

    :Exemple:

    >>> l = read_pokemon_csv()
    >>> train, test = split_pokemons(l)
    >>> m_m_values = min_max_values(train)
    >>> knn2(test[0], train, 3, m_m_values) in { True, False }
    True
    """
    indice_prop = POKE_PROP_NAMES.index(prop)
    classes_prop = {}
    liste_k_plus_proche = nearest_neighbors(pokemon, train_data, k,
                                            mini_maxi_values, distance)
    for cpl_dist_poke in liste_k_plus_proche:
        poke2 = cpl_dist_poke [1]
        classe_poke2 = poke2 [indice_prop]
        if not (classe_poke2 in classes_prop):
        # si clé non encore référencée
            classes_prop [classe_poke2] = 1
            # ajoute la clé dans le dictionnaire
        else:
            classes_prop [classe_poke2] += 1
            
    return sorted (classes_prop, key=classes_prop.get) [-1]
'''
max valeur d'un dict :
>>> d = {'a':10, 'b':50, 'c':15}
>>> max(d, key=d.get)  (pour avoir la valeur max)       d.get est équiv à lambda k:d[k]
>>> sorted (d, key=d.get)   (pour avoir une liste triée des clés)
'''
    

def knn_data(test_data, train_data, k,
             distance = pokemon_manhattan_distance,
             prop = 'is_legendary'):
    """
    :param pokemon: (Pokemon) the pokemon to be classified
    :param train_data: (list of Pokemon) the train dataset
    :param k: (int) number of neighbors
    :param distance: (function) distance function used
    :return: (float) proportion of well classified data 
    
    :Exemple:

    >>> l = read_pokemon_csv()
    >>> train, test = split_pokemons(l)
    >>> knn_data(test, train, 4) > 0.6
    True
    """
    res = 0
    mini_maxi_values = min_max_values(train_data)
    for p in test_data:
        cl = knn(p, train_data, k, mini_maxi_values, distance, prop)
        if cl == p._asdict()[prop]:
            res += 1
    return res / len(test_data)

if __name__ == '__main__':
    #import doctest
    #doctest.testmod()
    train = read_pokemon_csv('pokemon_train.csv')
    test = read_pokemon_csv('pokemon_test.csv')
    suspect1 = read_pokemon_csv('pokemon_suspect1.csv')
    suspect2 = read_pokemon_csv('pokemon_suspect2.csv')
    n = 5

    for dist in [pokemon_manhattan_distance, pokemon_euclidian_distance]:
        print('####### {} ########'.format(dist))
        
        print('******* train ********')
        tx_reuss = [knn_data(test, train, k, dist) for k in range (n, 20)]
        print ('Resultats : {}'.format(tx_reuss))
        tx_max = max(tx_reuss)
        tx_max_arr = round(tx_max, 4) * 100
        k = tx_reuss.index(tx_max) + n
        print ('Meilleur taux : {} %  ;  Meilleur k : {}'.format(tx_max_arr, k))
        
        print('******* suspect 1 ********')
        tx_reuss = [knn_data(test, suspect1, k, dist) for k in range (n, 20)]
        print ('Resultats : {}'.format(tx_reuss))
        tx_max = max(tx_reuss)
        tx_max_arr = round(tx_max, 4) * 100
        k = tx_reuss.index(tx_max) + n
        print ('Meilleur taux : {} %  ;  Meilleur k : {}'.format(tx_max_arr, k))
        
        print('******* suspect 2 ********')
        tx_reuss = [knn_data(test, suspect2, k, dist) for k in range (n, 20)]
        print ('Resultats : {}'.format(tx_reuss))
        tx_max = max(tx_reuss)
        tx_max_arr = round(tx_max, 4) * 100
        k = tx_reuss.index(tx_max) + n
        print ('Meilleur taux : {} %  ;  Meilleur k : {}'.format(tx_max_arr, k))