#Algorithme knn POKEMON

| Fichier                | Meilleur $`k`$ pour la distance euclidienne | Meilleur $`k`$ pour la distance de Manhattan |
|------------------------|:---------------------------------------------:|:----------------------------------------------:|
| `pokemon_test.csv`     |  9                                          | 9                                            |
| `pokemon_suspect1.csv` |  5                                          | 5                                            |
| `pokemon_suspect2.csv` |  10                                         | 7                                            |



| Fichier                | Meilleur $`k`$ pour la distance euclidienne | Meilleur $`k`$ pour la distance de Manhattan |
|------------------------|:---------------------------------------------:|:----------------------------------------------:|
| `pokemon_test.csv`     |  5                                          | 5                                            |
| `pokemon_suspect1.csv` |  5                                          | 5                                            |
| `pokemon_suspect2.csv` |  5                                         | 5                                            |

