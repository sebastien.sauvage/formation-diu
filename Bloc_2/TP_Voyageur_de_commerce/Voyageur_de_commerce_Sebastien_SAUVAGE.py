#!/usr/bin/python3
# -*- coding: utf-8 -*-

import TSP_biblio

def distance_entre_villes(tour):
    '''
    Déterminer les distances entre chaque ville prise deux à deux dans le tour
    proposé en paramètre.
    Renvoie une liste de liste (matrice n x n, où n est le nombre de villes à
    traiter)
    
    :param: (list) Liste de villes
    
    :renvoie:
    (list)
    '''
    distance2D = []
    for indice_ville_depart in range(len(tour)):
        distance_depart_fixe = []
        for indice_ville_arrivée in range(len(tour)):
            distance_depart_fixe.append(
                TSP_biblio.distance(tour, indice_ville_depart, indice_ville_arrivée)
                )
        distance2D.append(distance_depart_fixe)
    return distance2D

def ville_plus_proche(ville_fixee, liste_villes, distance2D):
    '''
    Renvoie l'indice (élément de liste_villes) de la ville fixée (dont l'indice
    dans distance2D est donné en paramètre), les distances des villes entre elles
    étant données dans le paramètre distance2D.
    
    :param:
    ville_fixee : (int) Indice dans la matrice distance2D de la ville fixée.
    liste_villes : (list) Liste d'indice des villes dont on veut étudier l'éloignement
    avec indice_ville.
    distance2D : Liste de listes (matrice) correspondant aux distances entre deux
    villes prises deux à deux.
    
    :renvoie:
    (int)
    
    :CU:
    Renvoie 0 si la liste liste_villes contient ville_fixee
    '''
    
    
    PREMIERE_VILLE = liste_villes[0]
    distance_mini = distance2D[ville_fixee][PREMIERE_VILLE]
    ville_plus_proche = PREMIERE_VILLE
    for ville in liste_villes:
        distance = distance2D[ville_fixee][ville]
        if distance < distance_mini:
            distance_mini = distance
            ville_plus_proche = ville
            
            # Corriger en utilisant la fonction min(list) ...
            
    return ville_plus_proche

def retire_ville_liste(ville, liste_villes):
    '''
    Renvoie le paramètre liste_villes dont on a retiré l'élément ville, donné en
    premier paramètre.
    
    :param:
    ville : (int) Indice de la ville à retirer.
    liste_villes : (list) Liste de villes dont on veut retirer un élément.
    
    :renvoie:
    (list)
    '''
    
    try :
        indice = liste_villes.index(ville)
        del liste_villes[indice]
    except ValueError:
        #Evite un renvoie d'erreur dans le cas où la ville n'est pas dans la liste.
        pass
    return liste_villes

def plus_proche_voisin(ville_depart, liste_villes, distance2D):
    '''
    Donne, selon la méthode gloutonne des plus proche voisins une tour des villes
    à partir de la ville_depart.

    :param:
    ville_depart : (int) Indice de la ville de départ.
    liste_villes : (list) Liste d'indice des villes dont on veut étudier l'éloignement
    avec ville_depart. Le paramètre contient la ville_depart.
    distance2D : (list) Liste de listes (matrice) correspondant aux distances entre deux
    villes prises deux à deux.
    
    :renvoie:
    (list)
    
    :Effet de bord:
    Modifie le contenu de la liste liste_villes
    '''
    # ELEMENTS DE CORRECTION :
    # Créer une fonction plus_proche(ville, liste_villes, ddistance2D)
    # et une fonction resoud(depart, a_parcourir, distance2D)
    # Penser à faire une copie de la liste donnée en paramètre pour être plus propre
    tour = [ville_depart]
    retire_ville_liste(ville_depart, liste_villes)
    ville_proche = 1
    while len(liste_villes) !=0:
        derniere_ville = tour[-1]
        ville_proche = ville_plus_proche(derniere_ville, liste_villes, distance2D)
        tour.append(ville_proche)
        liste_villes = retire_ville_liste(ville_proche, liste_villes)
    tour.append(ville_depart)
    return tour

def position_optimale(ville, tour):
    '''
    Renvoie l'indice (entre 1 et len(tour)) pour lequel l'insertion de la ville
    à cet indice permet d'obtenir le tour le plus court.
    
    :param:
    ville : (int) Ville que l'on souhaite insérer.
    tour : (list) Tour dans lequel on souhaite insérer la ville donné en paramètre.
    
    :renvoie:
    (list)
    '''
    #########################################
    # ne fonctionne pas/non testé
    position_optimale = len(tour)
    longueur_mini = TSP_biblio.longueur_tour(tour.append(ville))
    for position in range(1, len(tour)):
        tour_provisoir = inserer(ville, position, tour)
        longueur_tour_provisoir = TSP_biblio.longueur(tour_provisoir)
        if longueur_tour_provisoir < longueur_mini:
            longueur_mini = longueur_tour_provisoir
            position_optimale = position
    return position_optimale

def inserer(ville, indice, tour):
    '''
    Insère ville en position indice dans tour.
    
    :param:
    ville : (int) Ville à insérer.
    indice : (int) Indice où insérer la ville.
    tour : (list) Liste de villes.
    
    :renvoie:
    (list)
    '''
    #########################################
    # ne fonctionne pas/non testé
    return tour[:indice].append(ville)+(tour[indice:])

def insertion_proche(ville_depart, liste_villes, distance2D):
    '''
    Donne, selon la méthode gloutonne par la plus proche insertion la ville la
    plus proche une tour des villes à partir de la ville_depart.

    :param:
    ville_depart : (int) Indice de la ville de départ.
    liste_villes : (list) Liste d'indice des villes dont on veut étudier l'éloignement
    avec ville_depart. Le paramètre contient la ville_depart.
    distance2D : (list) Liste de listes (matrice) correspondant aux distances entre deux
    villes prises deux à deux.
    
    :renvoie:
    (list)
    
    :Effet de bord:
    Modifie le contenu de la liste liste_villes
    '''
    #########################################
    # ne fonctionne pas/non testé
    
    tour = [ville_depart]
    retire_ville_liste(ville_depart, liste_villes)
    while len(liste_villes) !=0:
        ville_suivante = liste_villes.pop(0)
        tour = inserer(ville_suivante,
                       position_optimale(ville_suivante, tour),
                       tour)
    tour.append(ville_depart)
    return tour
    






def liste_indices_en_liste_villes(liste_indices_villes):
    liste_villes = TSP_biblio.get_tour_fichier('exemple.txt')
    tour = []
    ordrevilles=[]
    for i in liste_indices_villes:
        tour.append(liste_villes[i])
        print ('Numéro : {:2}  Ville : {}'.format(i, liste_villes[i]))
        ordrevilles.append(liste_villes[i][0])
    print(ordrevilles)
    return tour

def affichage_liste_villes(liste_villes):
    TSP_biblio.trace(liste_indices_en_liste_villes(liste_villes))




if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
    liste_villes = TSP_biblio.get_tour_fichier('exemple.txt')
    distance_2D = distance_entre_villes(liste_villes)
    liste_indices_villes = [i for i in range(len(liste_villes))]
    #print(ville_plus_proche(0, [i for i in range(1, 10)], distance_2D))
    #print(plus_proche_voisin(0, liste_indices_villes, distance_2D))
    tour = plus_proche_voisin(14, liste_indices_villes, distance_2D)
    print('Longueur du tour : {}'.format(TSP_biblio.longueur_tour(liste_indices_en_liste_villes(tour))))
    
    
    '''
    dmin = TSP_biblio.longueur_tour(liste_indices_en_liste_villes(tour))
    villedep = 0
    for i in range(0, len(tour)-1):
        liste_indices_villes = [i for i in range(len(liste_villes))]
        tour = plus_proche_voisin(i, liste_indices_villes, distance_2D)
        d = TSP_biblio.longueur_tour(liste_indices_en_liste_villes(tour))
        print('Ville départ : {}   Longueur : {}'.format(i, d))
        if d<dmin:
            dmin = d
            villedep = i
    tour = plus_proche_voisin(villedep, liste_indices_villes, distance_2D)
    print('Longueur du tour : {}'.format(TSP_biblio.longueur_tour(liste_indices_en_liste_villes(tour))))
    '''
    
    
    #affichage_liste_villes(tour)
    
    #Instruction finale quand ça fonctionne :
    #affichage_liste_villes(insertion_proche(0, liste_indices_villes, distance_2D))
    