# Trame d'une fiche d'activité destinée aux élèves autour du Shell

## Sujet abordé
Cette fiche d'activités permettra de manipuler pour la première fois quelques instructions du Shell.

## Objectifs
* Histoire de l'informatique :
    * **Événements clés de l’histoire de l’informatique**
* Architectures matérielles et systèmes d’exploitation :
    * Systèmes d'exploitation
        * **Utiliser les commandes de base en ligne de commande.**

## Pré-requis
Cette activité fait suite à la notion de système d'exploitation et à leurs fonctions.
Les élèves se sont déjà connectés au moins une fois sous Linux, et connaissent l'interface graphique.
Ils ont déjà utilisé le Shell pour changer de mot de pass et connaissent uniquement la commande `passwrd`.
Les élèves ont déjà manipulé et organisé sous Windows des répertoires et fichiers et connaissent un principe simple de l'arborescence (fichiers et répertoires comme noeuds de l'arbre). La notion de fichiers cachés a déjà également été abordée.

## Préparation
* Première partie de cours théorique (45 minutes environ) : besoin d'un ordinateur et d'un vidéoprojecteur pour illustrer les propos et montrer les manipulations que les élèves devront pratiquer ensuite en seconde partie.
* La seconde partie (une heure et quart environ) s'effectuera en demi-classe et devant un ordinateur muni d'un système d'exploitation de type unix pour manipuler. A priori, il s'agira d'une distribution ubuntu dans mon lycée.

## Éléments de cours
* quelques explications sur l'intérêt de savoir manipuler le Shell malgré la présence également d'interfaces graphiques pourtant plus agréables ;
* système de fichiers, arborescence, liens physiques et symboliques, notion d'absolu et relatif.
* l'arborescence sous unix : répertoires `/`, `home`, `~$` ;
* le prompt du Shell ;
* manipulation des commandes de manière basique afin de dégrossir le TP qui est effectué en second temps.

## Séance pratique
Dans un second temps, les élèves disposent d'un ordinateur avec un système d'exploitation libre.
Travaux en petits exercices sur :
* la commande `ls` : manipulations simples à l'aide de différents paramètres (-a -l -la) et utilisation de `jokers`.
* la commande `man` : utilisation pour la commande `ls` afin d'en voir d'autres paramètres.
* Les commandes `tree`et `pwd` : une ou deux utilisations pour découvrir ces commandes.
* La commande `cd` : déplacement dans l'arborescence de l'espace personnel, puis déplacement guidé vers le public de la classe dans lequel une arborescence de fichiers aura préalablement été créée pour que les élèves puissent manipuler les commandes précédemment vues. Introduction alors de la commande `tree`.
* Toujours dans le répertoire public de la classe, utilisation de la commande `cat` pour visualiser le contenu de certains fichiers texte 
* Les commandes `touch` et `cat` : utilisation pour créer des fichiers texte avec un contenu simple : contenu quelconque puis contenu de type `CSV`.
* La commande `mkdir` : création d'une arborescence imposée par l'activité.
* Enfin, les commandes `cp`, `mv`et `rm` permettent de faire le ménage dans l'ensemble du répertoire personnel. 

* compléments éventuels :
    * notions de liens physiques et liens symboliques : commande `ln` ;
    * Création de répertoire et de sous-répertoires pour chacune des disciplines de l'élève. Dans le répertoire `nsi`, création d'une arborescence dans l'optique de création de pages web à l'avenir.

## Prolongement
* Suite à cette activité, des travaux personnels en groupe, hors classe, seront donnés afin de prendre en main les différentes commandes et leur manipulation.
* Mise en place d'un mémo commun par groupe (sur une page web ?).
* Ensuite, lors d'une autre activité, seront abordées les notions de droits d'accès aux fichiers, en particulier, en manipulant les commandes `chmod`, `chown` (Pour la seconde, à confirmer).
* Enfin, une fois la manipualtion bien maîtrisée, les élèves aborderont les redirections des entrées/sorties et les tâches (`|`, `ps`, `job`, `kill`, `bg` et `fg` pour l'essentiel)

## Evaluation des notions étudiées
* Dans un premier temps, utilisation des notions dans le cadre de questions flash en début de chaque séance :
    * une ou deux qcm sur le sujet ;
    * possibilité que les élèves puissent, à l'avance, faire des propositions de questions ;
    * les questions, une fois posées en classes sont disponibles via l'ENT ou tout autre espace de mise en commun des documents (espace à définir, peut-être un git externe).
* Travaux en DM sur l'usage des commandes (*A confirmer*)
* Après plusieurs semaines d'utilisation régulières en classe et via les questions flash, des questions apparaissent dans les devoirs surveillés.

## QCM E3C
**Question 1**\
*Quelle commande permet de créer un répertoire ?*
* mkdir
* ls
* cd
* tree

**Question 2**\
*Voici une arborescence :*
```bash
.
├── histoire_geographie
│   ├── cartes
│   └── documents
├── maths
│   ├── geometrie
│   └── python
│       └── fibonacci.py
├── nsi
└── physique_chimie
    ├── chimie
    └── physique
```
Dans cette dernière apparaît le mot `python`. Est-ce :
* un répertoire nommé python
* un fichier contenant un script python
* un programme nommé python
* une commande nommée python

**Question 3**\
*Voici une arborescence :*
```bash
.
├── histoire_geographie
│   ├── cartes
│   └── documents
├── maths
│   ├── geometrie
│   └── python
│       └── fibonacci.py
├── nsi
│   ├── algorithmique
│   ├── bases_programmation
│   └── web
└── physique_chimie
    ├── chimie
    └── physique
```
*Si vous vous trouvez dans le répertoire `web`. Quelle commande saisir pour afficher le contenu du répertoire `geometrie`?*\
* ls ../../maths/geometrie
* ls ../maths/geometrie
* ls geometrie
* ls /geometrie