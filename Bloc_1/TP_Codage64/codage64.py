# -*- coding: utf-8 -*-

# Auteur : Sébastien SAUVAGE
# Date juin 2019
# Version 1.0

import binary_IO

BASE64_SYMBOLS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                  'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                  'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                  'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                  'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                  'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                  'w', 'x', 'y', 'z', '0', '1', '2', '3',
                  '4', '5', '6', '7', '8', '9', '+', '/']

def to_base64(triplet):
    '''
    convertit le triplet d'octets en une chaîne de quatre symboles
    
    :param triplet: (tuple ou list) une séquence d'octets
    :return: (str) la chaîne de symboles de la base 64 représentant le triplet d'octets
    :CU: 1 <= len(triplet) <= 3 et les entiers de triplet tous compris entre 0 et 255
    :Exemple:
    
    >>> to_base64((18, 184, 156))
    'Eric'
    >>> to_base64((18, 184))
    'Erg='
    >>> to_base64((18,))
    'Eg=='
    '''
    
    n1 = triplet[0] # penser à traiter si pas 3 éléments
    try :
        n2 = triplet[1]
    except IndexError:
        n2 = 0
    try :
        n3 = triplet[2]
    except IndexError:
        n3 = 0
    
    sext = [0, 0, 0, 0, 0, 0]
    sext[0] = n1 >> 2
    sext[1] = ((n1 & 3) << 4) | (n2 >> 4)
    sext[2] = ((n2 & 15) << 2) | (n3 >> 6)
    sext[3] = (n3 & 63)
    codage = ''
    for indice in range(4):
        codage += BASE64_SYMBOLS[sext[indice]]
    if len(triplet) == 2:
        codage = codage[:-1] + "="
    elif len(triplet) == 1:
        codage = codage[:-2] + "=="
    return codage
    

def from_base64(b64_string):
    '''
    convertit une chaîne de quatre symboles en un tuple (le plus souvent triplet) d'octets
    
    :param b64_string: (str) une chaîne de symboles de la base 64
    :return: (tuple) un tuple d'octets dont b64_string est la représentation en base 64
    :CU: len(b64_string) == 4 et les caractères de b64_string sont dans la table ou le symbole =
    :Exemple:
    
    >>> from_base64('Eric')
    (18, 184, 156)
    >>> from_base64('Erg=')
    (18, 184)
    >>> from_base64('Eg==')
    (18,)
    '''
    
    #premier octet
    sext = []
    en_octets = []
    for index in range(2):
        sext.append((BASE64_SYMBOLS.index(b64_string[index])))
    en_octets.append((sext[0] << 2) | (sext[1] >> 4))
    
    #deuxième octet
    try :
        sext = []
        for index in range(1, 3):
            sext.append((BASE64_SYMBOLS.index(b64_string[index])))
        en_octets.append(((sext[0] << 4) & 255) | (sext[1] >> 2))
    except ValueError:
        pass
    
    #troisième octet
    try :
        sext =[]
        for index in range(2, 4):
            sext.append((BASE64_SYMBOLS.index(b64_string[index])))
        en_octets.append(((sext[0] << 6) & 255) | (sext[1]))
    except ValueError:
        pass
    
    return tuple(en_octets)

def  base64_encode(source):
    '''
 	Encode a file in base64 and outputs the result on standard output.

 	:param source: (str) the source filename
 	:return: None
 	:side effect: print on the standard output the base64 encoded version of the content 
                   of source file
    '''
    nb_sextet_dans_ligne = 0
    fichier_encode = ""
    fichier = binary_IO.Reader(source)
    triplet = fichier.get_bytes(3)
    while triplet != []:
        fichier_encode += (to_base64(triplet))
        nb_sextet_dans_ligne += 4
        if nb_sextet_dans_ligne ==76:
            nb_sextet_dans_ligne = 0
            fichier_encode += '\n'
        triplet = fichier.get_bytes(3)
    fichier.close()
    return fichier_encode

def base64_decode(source, cible):
    '''
    Decode a source file encoded in base64 and output the result.

    :param source: (str) the filename of the base64 file to decode
    :param cible: (str) filename of the file to produce
    :return: None
    :side effect: produce a new binary file
    '''
    nb_octets = 0
    fichier_src = binary_IO.Reader(source)
    fichier_dest = binary_IO.Writer(cible)
    quadruplet = fichier_src.get_bytes(4)
    nb_octets += 4
    while quadruplet !=[]:
        chaine_codee = ''
        for elmt in quadruplet:
            chaine_codee += chr(elmt)
        triplet_decode = from_base64(chaine_codee)
        fichier_dest.write_bytes(triplet_decode)
        if nb_octets ==76:
            quadruplet = fichier_src.get_bytes(1)
            nb_octets = 0
        quadruplet = fichier_src.get_bytes(4)
        nb_octets += 4
    fichier_dest.close()
    fichier_src.close()

if __name__ == "__main__":
    import doctest
    doctest.testmod()
    
    #print(to_base64((18, 184, 156)))
    #print(to_base64((18, 184)))
    #print(to_base64((18,)))
    
    #print(from_base64('Eric'))
    #print(from_base64('Erg='))
    #print(from_base64('Eg=='))
    
    #print(base64_encode('essai1.txt'))
    #print(base64_encode('essai2.txt'))
    #print(base64_encode('calbuth.png'))
    
    #base64_decode('essai1_code.txt', 'essai1_decode.txt')
    #base64_decode('essai2_code.txt', 'essai2_decode.txt')
    #base64_decode('calbuth.png_encode.txt', 'image_obtenue.png')