WA-TOR
======
Fiche projet
-----------
# Présentation
_Source : wikipédia (anglais) et introduction de la part des formateurs de l'activité WATOR lors du DIU-EIF à l'université de Lille._
>Wa-Tor est une simulation de type proie-prédateur. Dans une mer torique (un beignet avec un trou au milieu) évoluent des thons (les proies) et des requins (les prédateurs). Les uns et les autres se déplacent et se reproduisent. Pour acquérir l'énergie suffisante à sa survie un requin doit manger un thon régulièrement. Un thon vit éternellement tant qu'il n'est pas mangé par un requin.

>**_La mer_**

>La mer est représentée par une grille à deux dimensions torique. Chaque case a quatre voisines, une dans chacune des quatre directions cardinales (N, S, E O).

>Chaque case de cette grille représente une zone de mer qui peut être soit vide, soit occupée par un thon ou un requin.

>**_Les poissons_**

>Chaque thon est caractérisé par son temps de gestation. Ce temps est initialisé à une valeur initiale commune à tous les thons, appelée durée de gestation des thons.

>Chaque requin est caractérisé par son temps de gestation et son énergie. Ces deux valeurs sont initialisées à une valeur initiale commune à tous les requins, appelées respectivement durée de gestation des requins et énergie des requins.

>**_Simulation et comportements_**

>À chaque pas de la simulation, une case de la mer est sélectionnée aléatoirement. Si elle est vide, il ne se passe rien. Si elle est occupée par un poisson, on applique alors son comportement. Les comportements de thons et des requins sont régis par des règles simples.

>Un thon applique le comportement suivant :

>   **_Déplacement_**

>   Le thon choisit aléatoirement une case libre parmi ses voisines. S'il en existe une, le thon se déplace vers cette case. Il reste sur place sinon.

>   **_Reproduction_**

>   Le temps de gestation du thon est diminué de 1.

>   Si ce temps arrive à 0, le thon donne naissance à un nouveau thon qui nait sur la case qu'il vient de quitter s'il s'est déplacé. Sinon aucun thon ne nait. Le temps de gestation est remis à sa valeur initiale.

>Un requin applique le comportement suivant :

>   **_Energie_**

>   Le requin perd un point d'énergie.

>   **_Déplacement_**

>   Le requin choisit aléatoirement parmi ses voisines une case occupée par un thon. S'il en existe une, le requin se déplace vers cette case et mange le thon. Son niveau d'énergie est alors remis à sa valeur initiale. Sinon il cherche à se déplacer vers une case voisine vide choisie au hasard. Il reste sur place s'il n'y en a aucune.

>   Mort Si le niveau d'énergie du requin est à 0, il meurt. Dans ce cas l'étape suivante n'a évidemment pas lieu.

>   **_Reproduction_**

>   Le temps de gestation du requin est diminué de 1. Si ce temps arrive à 0, il donne naissance à un nouveau requin sur la case qu'il vient de quitter s'il s'est déplacé, sinon aucun reuqin ne nait. Son temps de gestation est remis à sa valeur initiale.

>**_Phénomènes proies-prédateurs émergents_**

>Les durées de gestation des deux espèces et l'énergie récupérée par un requin lorsqu'il mange un thon sont des paramètres de la simulation. S'ils sont bien choisis on peut voir émerger un phénomène "périodique" d'évolution des populations.

>Quand il y a peu de prédateurs, la population des proies augmente, mais cette abondance de proies permet alors aux prédateurs de facilement trouver l'énergie suffisante pour leur survie et leur reproduction. Leur population se met alors à croître au détriment de celle de leur proie. Quand ces dernières deviennent trop rares, les prédateurs finissent par mourir sans se reproduire. Et quand il y a peu de prédateurs...

>Pour obtenir ce phénomène cyclique,  il faut respecter les inégalités suivantes :

>temps gestation des thons < énergie des requins < durée gestation des requins

>Les valeurs suggérées sont 2, 3 et 4.

>Il est également souhaitable de débuter avec une configuration dans laquelle il y a un nombre de thons relativement important par rapport au nombre de requins. Par exemple 30% des cases de la mer sont initialement occupées par des thons et 10% par des requins.

# Objectifs
* Création de la grille
    * Fonction creation_grille
    * Fonction creation_thon
    * Fonction creation_requin
    * Fonction remplir_grille
    * Fonction afficher_grille
* Déplacement des thons
    * Fonction case_nord
    * Fonctions case_est, case_sud, case_ouest.
    * Fonction cases_libres
    * Fonction gestion_gestation
    * Fonction naissance
    * Fonction deplacement_vers_case_vide
    * Fonction deplacement_thon
* Déplacement des requins
    * Fonction cases_thons
    * Fonction deplacement_vers_thons
    * Fonction deplacement_requin
* Routine principale
    * Fonction decompte
    * Fonction graphique
    * Fonction routine_principale

# Création de la grille
**_Les paramètres de la grille et des poissons_**

Tout d'abord, même si les paramètres peuvent faire l'objet d'une variable locale dans la fonction principale, pour ce projet, elle seront stockées dans des **variables globales**.

TG_THON et TG_REQUIN seront les temps de gestation initial respectifs des deux types de poissons.

E_REQUIN sera l'énergie initiale de tout requin.

PROP_THON et PROP_REQUIN sont les proportions respectives des deux types de poissons lors de la création de la grille.

L'introduction donne les valeurs indicatives à donner à ces variables globales.

**_Objectif de la partie Création de la grille_**

Dans cette première partie, l'objectif est d'écrire différentes fonctions permettant de pouvoir disposer d'une grille contenant des thons et des poissons.

**_Structure des données_**

La grille assimilée à un tableau m x n sera une liste de n listes contenant m éléments. Ainsi, le poisson situé à la iè ligne et jè colonne sera grille[i][j]. Attention, il s'agit alors du poisson situé au point de coordonnées (j , i) (Inversion de l'ordre des variables). Un petit schéma suffit à s'en persuader.

Les contenus des cases de la gille seront également des listes. Elles sont de trois types : les cases vides, [], les thons, ['T', tps gestation (int)] et enfin les requins ['R', tps gestation (int), energie (int)].

## Fonction creation_grille

Ecrire une fonction appelée creation_grille, comportant deux paramètres col et lig. Cette fonction renvoie une grille comportant col colonnes et lig lignes. Chaque élément de la grille est une liste vide.

Exemples de tests :
```python
>>> creation_grille(2,2) ==[[[], []], [[], []]]
True
>>> creation_grille(4,3) ==[[[], [], [], []], [[], [], [], []], [[], [], [], []]]
True
>>> creation_grille(2,5) ==[[[], []], [[], []], [[], []], [[], []], [[], []]]
True
```

## Fonction creation_thon

Ecrire une fonction appelée creation_thon, ne comportant pas de paramètre. Cette fonction renvoie une liste à deux éléments, le premier étant la chaîne ‘T’, le second étant la valeur d’une variable globale TG_THON, qui correspond aux temps de gestation du thon. 

Le fait de scinder la création du thon de la grille permet de pouvoir l’appeler, à la fois dans la construction de la grille initiale et également lorsqu’un thon se reproduit.

Exemples de tests :
```python
>>> creation_thon() ==['T', 2]
True
```

## Fonction creation_requin

S’inspirer de la fonction creation_thon pour la fonction creation_requin, à la différence qu’il faut ajouter un troisième élément, le nombre d’énergie des requins, troisième variable globale supplémentaire que l’on nommera ER_REQUIN. Le temps de gestation du requin sera une variable globale nommée TG_REQUIN.

Exemples de tests :
```python
>>> creation_requin() ==['R', 4, 3]
True
```

## Fonction remplir_grille

Ecrire une fonction appelée remplir_grille, comportant comme paramètres une grille remplie de listes vides, la proportion de thons et la proportion de requins. Cette fonction renvoie la grille remplie.

## Fonction afficher

Ecrire une fonction appelée afficher, comportant comme paramètres une grille. La fonction affiche la grille en affichant T pour un thon, R pour un requin et _ pour une case vide.

Penser à ajouter deux lignes vides après l'affichage de la grille pour espacer les affichages successifs qui seront effectués ensuite.

On pourra utiliser cette fonction tout au long du projet pour s'aider à afficher les grilles renvoyées par les différentes fonctions à rédiger.

Exemples de tests :
```python
>>> afficher(creation_grille(3,3))
_ _ _ 
_ _ _ 
_ _ _ 
<BLANKLINE>
<BLANKLINE>
```

# Gestion des thons

Cette partie permet d'écrire différentes fonctions appelées pour gérer le contenu d'une case si celle-ci est un thon.

Déroulé :
* le thon perd un point de temps de gestation ;
* si son temps de gestation est à 0, il est initialisé à la valeur TG_THON ;
* on regarde si une case est libre pour se déplacer ;
* si une case est libre, on recopie la case où se trouve le thon dans la case d'arrivée (à ce moment là, il y a deux thons identiques, l'un est en trop) ;
* en cas de déplacement :
    * si la case initiale est un thon avec un temps de gestation égal à TG_THON (c'est à dire qu'il y a naissance), on laisse tel quel (puisque finalement, le thon situé dans la case initiale a les mêmes caractéristiques qu'un thon qui doit naître) ;
    * si la case initiale est un thon avec un temps de gestion différent de TG_THON (donc pas initialisé), on efface le contenu de la case, puisque le thon, ayant été déplacé, n'a plus rien à faire là.

Ce déroulé s'effectuera via les différentes fonctions ci-dessous à écrire. Normalement, même si le traitement s'effectue par différentes fonctions, les contenus des cases de la grille permettent de savoir ce qui s'est fait. Malgré tout, en cas de difficulté de gestion du déplacement des thons, vous pouvez utiliser (cela n'est pas précisé par la suite) des booléens supplémentaires en renvoie de certaines fonctions pour laisser une trace plus simple de ce qui s'est ou non fait (déplacement effectué ou non, naissance ou non, …).

## Fonction case_nord

Ecrire une fonction appelée cases_nord, comportant trois paramètres : la grille, ainsi que x et y, les coordonnées d’une case de la grille à tester. La fonction renvoie le contenu de la case située au nord de la case à tester.

Exemples de tests :
```python
>>> case_nord([[[], ['T', 5], [], []], [['R', 5, 4], [], ['R', 2, 4], []], [[], ['T', 2], [], []], [[], [], [], []]], 1, 1) ==['T', 5]
True
>>> case_nord([[[], [], [], []], [[], [], [], []], [[], [], [], []], [[], ['T', 5], [], []]], 1, 0) ==['T', 5]
True
```

## Fonctions case_est, case_sud, case_ouest

Ecrire trois fonctions similaires, nommées case_est, case_sud, case_ouest, pour renvoyer les contenus des trois autres cases adjacentes à celle testée.

Exemples de tests :
```python
>>> case_est([[[], ['T', 5], [], []], [['R', 5, 4], [], ['R', 2, 4], []], [[], ['T', 2], [], []], [[], [], [], []]], 1, 1) ==['R', 2, 4]
True
>>> case_est([[[], [], [], []], [['R', 2, 4], [], [], []], [[], [], [], []], [[], [], [], []]], 3, 1) ==['R', 2, 4]
True
Exemples de tests :
>>> case_sud([[[], ['T', 5], [], []], [['R', 5, 4], [], ['R', 2, 4], []], [[], ['T', 2], [], []], [[], [], [], []]], 1, 1) ==['T', 2]
True
>>> case_sud([[[], ['T', 2], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]], 1, 3) ==['T', 2]
True
>>> case_ouest([[[], ['T', 5], [], []], [['R', 5, 4], [], ['R', 2, 4], []], [[], ['T', 2], [], []], [[], [], [], []]], 1, 1) ==['R', 5, 4]
True
>>> case_ouest([[[], [], [], []], [[], [], [], ['R', 5, 4]], [[], [], [], []], [[], [], [], []]], 0, 1) ==['R', 5, 4]
True
```

## Fonction cases_libres

Ecrire une fonction appelée cases_libres, comportant trois paramètres : la grille, ainsi que x et y, les coordonnées du point de la grille autour duquel on souhaite connaître les possibilités de déplacement vers des cases vides. La fonction renvoie une liste contenant les différentes directions possibles sous forme de chaînes d’un caractère.

Exemples de tests :
```python
>>> cases_libres([[[], [], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]], 2, 1) ==['N', 'E', 'S', 'O']
True
>>> cases_libres([[[], [], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]], 2, 0) ==['N', 'E', 'S', 'O']
True
>>> cases_libres([[[], [], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]], 3, 1) ==['N', 'E', 'S', 'O']
True
>>> cases_libres([[[], [], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]], 2, 3) ==['N', 'E', 'S', 'O']
True
>>> cases_libres([[[], [], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]], 2, 0) ==['N', 'E', 'S', 'O']
True
>>> cases_libres([[[], ['T', 5], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==['E', 'S', 'O']
True
>>> cases_libres([[[], ['T', 5], [], []], [['R', 5, 4], [], ['R', 5, 4], []], [[], ['T', 2], [], []], [[], [], [], []]], 1, 1) ==[]
True
```

## Fonctions gestion_gestation

Ecrire une fonction admettant un paramètre poisson. Poisson est une liste de deux ou trois éléments correspondant au type de poisson à traiter : [‘T’, tg] ou [‘R’, tg, er]. Cette fonction renvoie cette liste modifiée en diminuant de 1 le deuxième élément et la réinitialise si le temps de gestation est nul.

Exemples de tests :
```python
>>> gestion_gestation(['T', 2]) ==['T', 1]
True
>>> gestion_gestation(['T', 1]) ==['T', 2]
True
>>> gestion_gestation(['R', 2, 3]) ==['R', 1, 3]
True
>>> gestion_gestation(['R', 1 , 3]) ==['R', 4, 3]
True
```

## Fonctions naissance

Ecrire une fonction admettant trois paramètres : la grille, x et y les coordonnées du poisson à traiter. Elle renvoie la grille modifiée. La fonction vide la case de coordonnées x et y si celle-ci ne doit pas contenir une naissance, que ce soit un thon ou un requin.

Exemples de tests :
```python
>>> naissance([[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]], 0, 0) ==[[[], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]]
True
>>> naissance([[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]], 1, 0) ==[[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]]
True
>>> naissance([[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]], 2, 0) ==[[['T', 1], ['T', 2], []], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]]
True
>>> naissance([[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]], 0, 1) ==[[['T', 1], ['T', 2], ['R', 3, 2]], [[], ['R', 1, 2], ['R', 4, 2]]]
True
>>> naissance([[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]], 1, 1) ==[[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], [], ['R', 4, 2]]]
True
>>> naissance([[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]], 2, 1) ==[[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]]
True
```

## Fonctions deplacement_vers_case_vide

Ecrire une fonction vers_case_vide admettant la grille, et les coordonnées du poisson traité en paramètre. La fonction renvoie la grille modifiée. Elle déplace le poisson traité dans l'une des cases vides autour de lui.

Exemples de tests :
```python
>>> deplacement_vers_case_vide([[[], [], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['T', 1], [], []], [['R', 1, 1], [], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
True
>>> deplacement_vers_case_vide([[[], [], [], []], [['R', 1, 1], ['T', 2], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['T', 2], [], []], [['R', 1, 1], ['T', 2], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
True
>>> deplacement_vers_case_vide([[[], [], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['T', 1], [], []], [['R', 1, 1], [], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
True
>>> deplacement_vers_case_vide([[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 1], [], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['R', 1, 1], [], ['T', 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
True
>>> deplacement_vers_case_vide([[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['R', 1, 1], [], ['R', 1, 1], []], [[], ['T', 1], [], []], [[], [], [], []]]
True
>>> deplacement_vers_case_vide([[[], ['R', 1, 1], [], []], [[], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['T', 1], [], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
True
>>> deplacement_vers_case_vide([[['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []], [[], [], [], []]], 1, 0) ==[[['R', 1, 1], [], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []], [[], ['T', 1], [], []]]
True
>>> deplacement_vers_case_vide([[[], [], [], ['R', 1, 1]], [[], [], ['R', 1, 1], ['T', 1]], [[], [], [], ['R', 1, 1]], [[], [], [], []]], 3, 1) ==[[[], [], [], ['R', 1, 1]], [['T', 1], [], ['R', 1, 1], []], [[], [], [], ['R', 1, 1]], [[], [], [], []]]
True
>>> deplacement_vers_case_vide([[[], [], [], []], [[], [], [], []], [[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []]], 1, 3) ==[[[], ['T', 1], [], []], [[], [], [], []], [[], ['R', 1, 1], [], []], [['R', 1, 1], [], ['R', 1, 1], []]]
True
>>> deplacement_vers_case_vide([[['R', 1, 1], [], [], []], [['T', 1], ['R', 1, 1], [], []], [['R', 1, 1], [], [], []], [[], [], [], []]], 0, 1) ==[[['R', 1, 1], [], [], []], [[], ['R', 1, 1], [], ['T', 1]], [['R', 1, 1], [], [], []], [[], [], [], []]]
True
>>> deplacement_vers_case_vide([[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
True
```

## Fonction deplacement_thon

Ecrire une fonction de paramètres grille, x et y les coordonnées du poisson à traiter. La fonction appelle les fonctions précédentes pour déplacer le thon et en faire naître un si besoin.

Exemples de tests :
```python
>>> deplacement_thon([[[], [], [], []], [['R', 1, 1], ['T', 2], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['T', 1], [], []], [['R', 1, 1], [], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
True
>>> deplacement_thon([[[], [], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['T', 2], [], []], [['R', 1, 1], ['T', 2], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
True
>>> deplacement_thon([[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 2], [], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['R', 1, 1], [], ['T', 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
True
>>> deplacement_thon([[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 2], ['R', 1, 1], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['R', 1, 1], [], ['R', 1, 1], []], [[], ['T', 1], [], []], [[], [], [], []]]
True
>>> deplacement_thon([[[], ['R', 1, 1], [], []], [[], ['T', 2], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['T', 1], [], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
True
>>> deplacement_thon([[['R', 1, 1], ['T', 2], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []], [[], [], [], []]], 1, 0) ==[[['R', 1, 1], [], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []], [[], ['T', 1], [], []]]
True
>>> deplacement_thon([[[], [], [], ['R', 1, 1]], [[], [], ['R', 1, 1], ['T', 2]], [[], [], [], ['R', 1, 1]], [[], [], [], []]], 3, 1) ==[[[], [], [], ['R', 1, 1]], [['T', 1], [], ['R', 1, 1], []], [[], [], [], ['R', 1, 1]], [[], [], [], []]]
True
>>> deplacement_thon([[[], [], [], []], [[], [], [], []], [[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 2], ['R', 1, 1], []]], 1, 3) ==[[[], ['T', 1], [], []], [[], [], [], []], [[], ['R', 1, 1], [], []], [['R', 1, 1], [], ['R', 1, 1], []]]
True
>>> deplacement_thon([[['R', 1, 1], [], [], []], [['T', 2], ['R', 1, 1], [], []], [['R', 1, 1], [], [], []], [[], [], [], []]], 0, 1) ==[[['R', 1, 1], [], [], []], [[], ['R', 1, 1], [], ['T', 1]], [['R', 1, 1], [], [], []], [[], [], [], []]]
True
>>> deplacement_thon([[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 2], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
True
```

# Gestion des requins

Cette partie permet d'écrire différentes fonctions appelées pour gérer le contenu d'une case si celle-ci est un requin. La difficulté ici sera l'ajout d'une seconde variable liée au requin (son énergie) ainsi que le fait qu'un requin perd de suite un point d'énergie mais ne meurt que s'il ne s'est pas déplacé vers un thon.

Une fonction de déplacement du requin vers une case vide n'est pas à refaire, puisque nous allons utiliser celle de la partie précédente pour le déplacement des thons.

Déroulé :
* le requin perd un point d'énergie (qui peut donc être égal à 0) ;
* le requin perd un point de temps de gestation ;
* si son temps de gestation est à 0, il est initialisé à la valeur TG_REQUIN;
* on regarde si une case voisine contient un thon pour se déplacer ;
* si une case voisine contient un thon, le requin initialise son énergie puis on recopie la case initiale dans celle contenant le thon (à ce moment là, il y a deux requins identiques, l'un est en trop) ;
* en cas de déplacement (vers un thon) :
    * si la case initiale est un requin avec un temps de gestation égal à TG_REQUIN (c'est à dire qu'il y a naissance), on laisse tel quel (puisque finalement, le requin situé dans la case initiale a les mêmes caractéristiques qu'un requin qui doit naître, c'est à dire avec un temps de gestation et une énergie initialisés) ;
    * si la case initiale est un requin avec un temps de gestion différent de TG_REQUIN (donc pas initialisé), on efface le contenu de la case, puisque le requin, ayant été déplacé, n'a plus rien à faire là (le requin a mangé un thon mais n'a pas donné naissance donc laisse une case vide à son emplacement initial).
* si la case initiale contient un requin avec une énergie nulle, on efface le contenu de la case (cas où le requin n'a pas mangé de thon) ;
* si la case n'est pas vide et si son énergie n'est pas égale à la valeur initiale (sinon cela veut dire que le requin a donné naissance et donc n'a plus à se déplacer), on regarde si une case vide est libre pour se déplacer ;
* à partir de ce point, on traite le déplacement du requin de la même manière que celle du thon.

Ce déroulé s'effectuera via les différentes fonctions ci-dessous à écrire et celles déjà écrites pour le déplacement des thons. Normalement, même si le traitement s'effectue par différentes fonctions, les contenus des cases de la grille permettent de savoir ce qui s'est fait. Malgré tout, en cas de difficulté de gestion du déplacement des requins, vous pouvez utiliser (cela n'est pas précisé par la suite) des booléens supplémentaires en renvoie de certaines fonctions pour laisser une trace plus simple de ce qui s'est ou non fait (déplacement effectué ou non, naissance ou non, …).

## Fonction cases_thons

Ecrire une fonction appelée cases_thons, qui est similaire à la fonction cases_libres mais qui renvoie une liste de direction autour d'un requin comportant des directions.

Exemples de tests :
```python
>>> cases_thon([[[], ['T', 1], [], []], [['T', 1], ['R', 2, 1], ['T', 1], []], [[], ['T', 1], [], []], [[], [], [], []]], 1, 1) ==['N', 'E', 'S', 'O']
True
>>> cases_thon([[[], [], [], []], [['T', 1], ['R', 2, 1], ['T', 1], []], [[], ['T', 1], [], []], [[], [], [], []]], 1, 1) ==['E', 'S', 'O']
True
>>> cases_thon([[[], [], [], []], [['T', 1], ['R', 2, 1], [], []], [[], ['T', 1], [], []], [[], [], [], []]], 1, 1) ==['S', 'O']
True
>>> cases_thon([[[], [], [], []], [['T', 1], ['R', 2, 1], [], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==['O']
True
>>> cases_thon([[[], [], [], []], [[], ['R', 2, 1], [], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==[]
True
```

## Fonction deplacement_vers_thon et deplacement_requin

Ecrire des fonctions appelées deplacement_vers_thons, et deplacement_requin en s'inspirant du travail effectué pour les thons. Certaines fonctions écrites pour les thons comme cases_libres peuvent être réutilisées pour les requins. Penser à ajouter la gestion de l'énergie des requins qui n'existe pas pour les thons.

Attention à bien gérer cet élément : une énergie à zéro ne fait disparaître le requin que si ce dernier ne s'est pas déplacé vers un thon.

Exemples de tests :
```python
>>> deplacement_vers_thon([[[], ['T', 1], [], []], [[], ['R', 2, 3], [], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 2, 3], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]]
True
>>> deplacement_vers_thon([[[], [], [], []], [[], ['R', 2, 3], ['T', 1], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==[[[], [], [], []], [[], [], ['R', 2, 3], []], [[], [], [], []], [[], [], [], []]]
True
>>> deplacement_vers_thon([[[], [], [], []], [[], ['R', 2, 3], [], []], [[], ['T', 1], [], []], [[], [], [], []]], 1, 1) ==[[[], [], [], []], [[], [], [], []], [[], ['R', 2, 3], [], []], [[], [], [], []]]
True
>>> deplacement_vers_thon([[[], [], [], []], [['T', 1], ['R', 2, 3], [], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==[[[], [], [], []], [['R', 2, 3], [], [], []], [[], [], [], []], [[], [], [], []]]
True
>>> deplacement_vers_thon([[[], [], [], []], [[], ['R', 2, 3], [], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==[[[], [], [], []], [[], ['R', 2, 3], [], []], [[], [], [], []], [[], [], [], []]]
True
```

Exemples de tests :
```python
>>> deplacement_requin([[[], ['T', 2], []], [[], ['R', 4, 3], []], [[], [], []]], 1, 1) ==[[[], ['R', 3, 3], []], [[], [], []], [[], [], []]]
True
>>> deplacement_requin([[[], ['T', 2], []], [[], ['R', 1, 3], []], [[], [], []]], 1, 1) ==[[[], ['R', 4, 3], []], [[], ['R', 4, 3], []], [[], [], []]]
True
>>> deplacement_requin([[[], ['T', 2], []], [[], ['R', 2, 0], []], [[], [], []]], 1, 1) ==[[[], ['R', 1, 3], []], [[], [], []], [[], [], []]]
True
```

# Routine principale

Maintenant que la grille est définie, que les thons et les requins peuvent se déplacer, il ne reste plus qu'à appeler ces fonctions pour effectuer la simulation.

Outre le fait que la simulation comptera 10 000 répétitions, nous allons également stocker à chaque pas les effectifs des deux catégories de poissons afin de tracer les évolutions des espèces en fonctions du temps.

## Fonction decompte

Ecrire une fonction appelée decompte, avec un paramètre qui est la grille. La fonction renvoie une liste à deux éléments, le premier étant le nombre de thons dans la grille, le second, le nombre de thons.

Exemples de tests :
```python
>>> decompte([[[], [], [], ['T', 2]], [['T', 2], [], [], []], [['R', 4, 3], ['T', 2], ['T', 2], ['R', 4, 3]], [['T', 2], [], [], []]]) ==[5, 2]
True
```

## Fonction graphique

Ecrire une fonction appelée graphique admettant une liste de trois listes comme paramètre. Le premier élément de la liste correspond aux abscisses des points à tracer. Les deux autres éléments de la liste correspondent aux ordonnées des deux points de même abscisse à placer. Le graphique utilise la bibliothèque pylab (ou numpy et mathlab) pour afficher l'évolution des effectifs des poissons en fonction du temps. La fonction ne renvoie rien.

## Fonction routine_principale

Ecrire une fonction routine_principale qui prend une grille comme paramètre. La fonction effectuera 10 000 fois les mêmes tâches :
* compter les poissons ;
* ajouter ces valeurs dans une liste de type [liste d'abscisses, liste de nb de thons, liste de nb de requins] ;
* gérer le contenu d'une des cases de la grille prise au hasard (déplacement d'un thon ou d'un requin si la case n'est pas vide).

Toutes les 100 répétitions, la grille sera affichée avec une pause lors de cet affichage (utiliser la fonction sleep du module time).

Remarque : au lieu de répéter 10 000 fois la même chose, on peut également effectuer une boucle tant qu'il reste des poissons (jusqu'à 10 000 répétitions maximum quand même) afin d'éviter le traitement de grilles vides inutilement.

# Quelques évolutions de populations avec les paramètres initiaux
Temps de gestation du thon : 1 ;

Temps de gestation du requin : 3 ;

Energie initiale d'un requin : 7 ;

Proportion initiale de thons : 30 % ;

Proportion intiale de requins : 10 %.

![Premier exemple d'évolution de populations](Bloc_1/Projet wator/WATOR 1-3-7-30-10_sim 1.png)

Temps de gestation du thon : 2 ;

Temps de gestation du requin : 3 ;

Energie initiale d'un requin : 7 ;

Proportion initiale de thons : 30 % ;

Proportion intiale de requins : 10 %.

![Deuxième exemple d'évolution des populations](Bloc_1/Projet wator/WATOR 2-3-7-30-10_sim 1.png)
![Troisième exemple d'évolution des populations](Bloc_1/Projet wator/WATOR 2-3-7-30-10_sim 2.png)

Temps de gestation du thon : 2 ;

Temps de gestation du requin : 3 ;

Energie initiale d'un requin : 4 ;

Proportion initiale de thons : 30 % ;

Proportion intiale de requins : 10 %.

![Quatrième exemple d'évolution des populations](Bloc_1/Projet wator/WATOR 2-3-4-30-10_sim 1.png)
![Dernier exemple d'évolution des populations](Bloc_1/Projet wator/WATOR 2-3-4-30-10_sim 2.png)
