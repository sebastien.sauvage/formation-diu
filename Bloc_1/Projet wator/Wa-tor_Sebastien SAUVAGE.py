#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:author: Sébastien SAUVAGE <sebastien.sauvage@ac-lille.fr>
:date: mai 2019

Projet WA-TOR dans le cadre du bloc 1 du DIU EIL


Pour lancer l'execution sur une grille 10 x 10, saisir :
routine_principale(remplir_grille(creation_grille()))

Pour lancer l'execution sur une grille m x n, saisir :
routine_principale(remplir_grille(creation_grille(m, n)))

"""

import random
import time
import numpy
import matplotlib.pyplot as plt

#################################
# Paramètres des thons et requins
TG_THON = 2
E_REQUIN = 3
TG_REQUIN = 4

P_THONS = 30 / 100
P_REQUINS = 10 / 100

coordonnees = [[], [], []]

########################
# Création de la grille

def creation_grille(col = 10, lig = 10):
    '''
    Renvoie une grille contenant des listes vides. La grille comporte col colonnes
    et lig lignes.

    :param:
    col : (int) nombre de colonnes de la grille
    lig : (int) nombre de lignes de la grille
    :renvoie:
    grille : (list)

    >>> creation_grille(2,2) ==[[[], []], [[], []]]
    True
    >>> creation_grille(4,3) ==[[[], [], [], []], [[], [], [], []], [[], [], [], []]]
    True
    >>> creation_grille(2,5) ==[[[], []], [[], []], [[], []], [[], []], [[], []]]
    True
    '''
    grille = []
    for _ in range (lig):
        ligne_vide = []
        for __ in range (col):
            ligne_vide.append([])
        grille.append(ligne_vide)
    return grille

def creation_thon():
    '''
    Renvoie une liste à deux éléments, le premier 'T', le second étant le temps
    de gestation du thon (TG_THON).

    :param:
    (NoneType)
    
    :renvoie:
    (list)

    >>> creation_thon() ==['T', 2]
    True
    '''
    global TG_THON
    return ['T', TG_THON]

def creation_requin():
    '''
    Renvoie une liste à trois éléments, le premier 'R', le deuxième étant le temps
    de gestation du requin (TG_REQUIN) et le troisième étant l'énergie d'un requin
    (E_REQUIN)

    :param:
    (NoneType)
    
    :renvoie:
    (list)

    >>> creation_requin() ==['R', 4, 3]
    True
    '''
    global TG_REQUIN, E_REQUIN
    return ['R', TG_REQUIN, E_REQUIN]

def remplir_grille(grille, prob_thon = P_THONS, prob_requin = P_REQUINS):
    '''
    Parcours la grille, élément par élement et y place, soit un thon (avec une proportion
    donnée en paramètre), soit un requin (avec une proportion donnée en paramètre),
    soit rien.

    :param:
    grille : (list)
    prob_thon : (float) Proportion de thons
    prob_requin : (float) Proportion de requins

    :renvoie:
    (list)

    :conditions d'utilisation:
    prob_thon + prob_requin <=1

    :effet de bord:
    Modifie le paramètre grille avant de le renvoyer.
    '''
    nb_cases = len(grille) * len(grille[0])
    elmts = []
    for num in range (nb_cases):
        if num < prob_thon * nb_cases:
            elmts.append(creation_thon())
        elif num < (prob_thon + prob_requin) * nb_cases:
            elmts.append(creation_requin())
        else:
            elmts.append([])
    random.shuffle(elmts)
    for lig in range(len(grille)):
        for col in range(len(grille[0])):
            grille[lig][col] = elmts.pop()

    return grille

def afficher(grille):
    '''
    Affiche la grille dans la console.
    Affiche un T pour un thon.
    Affiche un R pour un requin.
    Affiche _ pour une case sans poison.
    
    :param:
    grille : (list) Contient la grille à afficher.
    
    :renvoie:
    (list)
    
    :effet de bord:
    Modifie la liste donnée en paramètre
    
    >>> afficher(creation_grille(3,3))
    _ _ _ 
    _ _ _ 
    _ _ _ 
    <BLANKLINE>
    <BLANKLINE>
    >>>
    '''
    for ligne in grille:
        for col in ligne:
            if col == []:
                print ('_', end = ' ')
            else:
                print(col[0], end= ' ')
        print('')
    print('')
    print('')

def case_nord(grille, x, y):
    '''
    Fonction qui renvoie le contenu de la case située au nord du point de coordonnées
    (x ; y).

    :param:
    grille : (list)
    x : (int) abscisse de la case à tester.
    y : (int) ordonnée de la case à tester.

    :renvoie:
    (list)

    >>> case_nord([[[], ['T', 5], [], []], [['R', 5, 4], [], ['R', 2, 4], []], [[], ['T', 2], [], []], [[], [], [], []]], 1, 1) ==['T', 5]
    True
    >>> case_nord([[[], [], [], []], [[], [], [], []], [[], [], [], []], [[], ['T', 5], [], []]], 1, 0) ==['T', 5]
    True
    '''
    return grille[y - 1][x]

def case_est(grille, x, y):
    '''
    Fonction qui renvoie le contenu de la case située à l'est du point de coordonnées
    (x ; y).

    :param:
    grille : (list)
    x : (int) abscisse de la case à tester.
    y : (int) ordonnée de la case à tester.
    
    :renvoie:
    (list)

    >>> case_est([[[], ['T', 5], [], []], [['R', 5, 4], [], ['R', 2, 4], []], [[], ['T', 2], [], []], [[], [], [], []]], 1, 1) ==['R', 2, 4]
    True
    >>> case_est([[[], [], [], []], [['R', 2, 4], [], [], []], [[], [], [], []], [[], [], [], []]], 3, 1) ==['R', 2, 4]
    True
    '''
    return grille[y][(x + 1) % len(grille)]

def case_sud(grille, x, y):
    '''
    Fonction qui renvoie le contenu de la case située au sud du point de coordonnées
    (x ; y).

    :param:
    grille : (list)
    x : (int) abscisse de la case à tester.
    y : (int) ordonnée de la case à tester.
    
    :renvoie:
    (list)

    >>> case_sud([[[], ['T', 5], [], []], [['R', 5, 4], [], ['R', 2, 4], []], [[], ['T', 2], [], []], [[], [], [], []]], 1, 1) ==['T', 2]
    True
    >>> case_sud([[[], ['T', 2], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]], 1, 3) ==['T', 2]
    True
    '''
    return grille[(y + 1) % len(grille)][x]

def case_ouest(grille, x, y):
    '''
    Fonction qui renvoie le contenu de la case située à l'ouest du point de coordonnées
    (x ; y).

    :param:
    grille : (list)
    x : (int) abscisse de la case à tester.
    y : (int) ordonnée de la case à tester.
    
    :renvoie:
    (list)

    >>> case_ouest([[[], ['T', 5], [], []], [['R', 5, 4], [], ['R', 2, 4], []], [[], ['T', 2], [], []], [[], [], [], []]], 1, 1) ==['R', 5, 4]
    True
    >>> case_ouest([[[], [], [], []], [[], [], [], ['R', 5, 4]], [[], [], [], []], [[], [], [], []]], 0, 1) ==['R', 5, 4]
    True
    '''
    return grille[y][x - 1]

def cases_libres(grille, x, y):
    '''
    Fonction qui renvoie une liste des directions dans lesquelles les cases sont
    vides autour du point de coordonnées (x ; y).

    :param:
    grille : (list)
    x : (int) abscisse du point autour duquel on cherche les cases vides.
    y : (int) ordonnée du point autour duquel on cherche les cases vides.
    
    :renvoie:
    (list)

    >>> cases_libres([[[], [], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]], 2, 1) ==['N', 'E', 'S', 'O']
    True
    >>> cases_libres([[[], [], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]], 2, 0) ==['N', 'E', 'S', 'O']
    True
    >>> cases_libres([[[], [], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]], 3, 1) ==['N', 'E', 'S', 'O']
    True
    >>> cases_libres([[[], [], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]], 2, 3) ==['N', 'E', 'S', 'O']
    True
    >>> cases_libres([[[], [], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]], 2, 0) ==['N', 'E', 'S', 'O']
    True
    >>> cases_libres([[[], ['T', 5], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==['E', 'S', 'O']
    True
    >>> cases_libres([[[], ['T', 5], [], []], [['R', 5, 4], [], ['R', 5, 4], []], [[], ['T', 2], [], []], [[], [], [], []]], 1, 1) ==[]
    True
    '''
    rep = []
    if case_nord(grille, x, y) ==[]:
        rep.append('N')
    if case_est(grille, x, y) ==[]:
        rep.append('E')
    if case_sud(grille, x, y) ==[]:
        rep.append('S')
    if case_ouest(grille, x, y) ==[]:
        rep.append('O')
    return rep

def gestion_gestation (poisson):
    '''
    Fonction qui diminue de 1 le deuxième élément de la liste poisson et le
    réinitialise si cette valeur devient nulle.

    :param:
    poisson : (list) poisson représenté sous la forme ['T', tg] ou ['R', tg, er]
    
    :renvoie:
    (list)
    
    :effet de bord:
    modifie le deuxième élément de la liste poisson.

    >>> gestion_gestation(['T', 2]) ==['T', 1]
    True
    >>> gestion_gestation(['T', 1]) ==['T', 2]
    True
    >>> gestion_gestation(['R', 2, 3]) ==['R', 1, 3]
    True
    >>> gestion_gestation(['R', 1 , 3]) ==['R', 4, 3]
    True
    '''
    global TG_THON, TG_REQUIN, E_REQUIN
    poisson[1] -= 1
    if poisson[0] =='T' and poisson[1] ==0:
        poisson[1] = TG_THON
    elif poisson[0] =='R' and poisson[1] ==0:
        poisson[1] = TG_REQUIN
        poisson[2] = E_REQUIN
    return poisson

def naissance(grille, x, y):
    '''
    Fonction qui vide la case de la grille de coordonnées x et y si cette dernière
    contenait un poisson qui n'a pas donné naissance.

    :param:
    grille : (list)
    x : (int) abscisse de la case du thon qui se déplace.
    y : (int) ordonnée de la case du thon qui se déplace.

    :renvoie:
    (List)

    :effet de bord:
    Modifie la liste donnée en paramètre

    >>> naissance([[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]], 0, 0) ==[[[], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]]
    True
    >>> naissance([[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]], 1, 0) ==[[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]]
    True
    >>> naissance([[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]], 2, 0) ==[[['T', 1], ['T', 2], []], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]]
    True
    >>> naissance([[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]], 0, 1) ==[[['T', 1], ['T', 2], ['R', 3, 2]], [[], ['R', 1, 2], ['R', 4, 2]]]
    True
    >>> naissance([[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]], 1, 1) ==[[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], [], ['R', 4, 2]]]
    True
    >>> naissance([[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]], 2, 1) ==[[['T', 1], ['T', 2], ['R', 3, 2]], [['R', 2, 2], ['R', 1, 2], ['R', 4, 2]]]
    True
    '''
    global TG_THON, TG_REQUIN
    if ((grille[y][x][0] =='T' and grille[y][x][1] <TG_THON) or
        (grille[y][x][0] =='R' and grille[y][x][1] <TG_REQUIN)):
        grille[y][x] = []
    return grille

def deplacement_vers_case_vide(grille, x, y):
    '''
    Fonction qui déplace, dans la mesure du possible, de manière aléatoire, un
    thon dans une case libre. Elle appelle la fonction naissance si en cas de
    déplacement, une naissance a lieu.

    :param:
    grille : (list)
    x : (int) abscisse de la case du thon qui se déplace.
    y : (int) ordonnée de la case du thon qui se déplace.

    :renvoie:
    (List)

    :effet de bord:
    Modifie la liste donnée en paramètre

    >>> deplacement_vers_case_vide([[[], [], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['T', 1], [], []], [['R', 1, 1], [], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
    True
    >>> deplacement_vers_case_vide([[[], [], [], []], [['R', 1, 1], ['T', 2], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['T', 2], [], []], [['R', 1, 1], ['T', 2], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
    True
    >>> deplacement_vers_case_vide([[[], [], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['T', 1], [], []], [['R', 1, 1], [], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
    True
    >>> deplacement_vers_case_vide([[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 1], [], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['R', 1, 1], [], ['T', 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
    True
    >>> deplacement_vers_case_vide([[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['R', 1, 1], [], ['R', 1, 1], []], [[], ['T', 1], [], []], [[], [], [], []]]
    True
    >>> deplacement_vers_case_vide([[[], ['R', 1, 1], [], []], [[], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['T', 1], [], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
    True
    >>> deplacement_vers_case_vide([[['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []], [[], [], [], []]], 1, 0) ==[[['R', 1, 1], [], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []], [[], ['T', 1], [], []]]
    True
    >>> deplacement_vers_case_vide([[[], [], [], ['R', 1, 1]], [[], [], ['R', 1, 1], ['T', 1]], [[], [], [], ['R', 1, 1]], [[], [], [], []]], 3, 1) ==[[[], [], [], ['R', 1, 1]], [['T', 1], [], ['R', 1, 1], []], [[], [], [], ['R', 1, 1]], [[], [], [], []]]
    True
    >>> deplacement_vers_case_vide([[[], [], [], []], [[], [], [], []], [[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []]], 1, 3) ==[[[], ['T', 1], [], []], [[], [], [], []], [[], ['R', 1, 1], [], []], [['R', 1, 1], [], ['R', 1, 1], []]]
    True
    >>> deplacement_vers_case_vide([[['R', 1, 1], [], [], []], [['T', 1], ['R', 1, 1], [], []], [['R', 1, 1], [], [], []], [[], [], [], []]], 0, 1) ==[[['R', 1, 1], [], [], []], [[], ['R', 1, 1], [], ['T', 1]], [['R', 1, 1], [], [], []], [[], [], [], []]]
    True
    >>> deplacement_vers_case_vide([[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
    True
    '''
    cases_dispos = cases_libres(grille, x, y)
    if len(cases_dispos) !=0:
        direction = cases_dispos[random.randrange(0, len(cases_dispos))]
        if direction =='N':
            grille[y - 1][x] = grille[y][x][:]
        elif direction =='E':
            grille[y][(x + 1) % len(grille[0])] = grille[y][x][:]
        elif direction =='S':
            grille[(y + 1) % len(grille)][x] = grille[y][x][:]
        else:
            grille[y][x - 1] = grille[y][x][:]
        grille = naissance(grille, x, y)[:]
    return grille

def deplacement_thon(grille, x, y):
    '''
    Fonction qui est appelée si la case choisie au hasard contient un thon.
    Elle appelle les fonctions gestion_gestation et deplacement_case_vide.

    :param:
    grille : (list)
    x : (int) abscisse de la case du thon qui se déplace.
    y : (int) ordonnée de la case du thon qui se déplace.

    :renvoie:
    (list)

    :effet de bord:
    Modifie la liste donnée en paramètre

    >>> deplacement_thon([[[], [], [], []], [['R', 1, 1], ['T', 2], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['T', 1], [], []], [['R', 1, 1], [], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
    True
    >>> deplacement_thon([[[], [], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['T', 2], [], []], [['R', 1, 1], ['T', 2], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
    True
    >>> deplacement_thon([[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 2], [], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['R', 1, 1], [], ['T', 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
    True
    >>> deplacement_thon([[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 2], ['R', 1, 1], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['R', 1, 1], [], ['R', 1, 1], []], [[], ['T', 1], [], []], [[], [], [], []]]
    True
    >>> deplacement_thon([[[], ['R', 1, 1], [], []], [[], ['T', 2], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['T', 1], [], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
    True
    >>> deplacement_thon([[['R', 1, 1], ['T', 2], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []], [[], [], [], []]], 1, 0) ==[[['R', 1, 1], [], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []], [[], ['T', 1], [], []]]
    True
    >>> deplacement_thon([[[], [], [], ['R', 1, 1]], [[], [], ['R', 1, 1], ['T', 2]], [[], [], [], ['R', 1, 1]], [[], [], [], []]], 3, 1) ==[[[], [], [], ['R', 1, 1]], [['T', 1], [], ['R', 1, 1], []], [[], [], [], ['R', 1, 1]], [[], [], [], []]]
    True
    >>> deplacement_thon([[[], [], [], []], [[], [], [], []], [[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 2], ['R', 1, 1], []]], 1, 3) ==[[[], ['T', 1], [], []], [[], [], [], []], [[], ['R', 1, 1], [], []], [['R', 1, 1], [], ['R', 1, 1], []]]
    True
    >>> deplacement_thon([[['R', 1, 1], [], [], []], [['T', 2], ['R', 1, 1], [], []], [['R', 1, 1], [], [], []], [[], [], [], []]], 0, 1) ==[[['R', 1, 1], [], [], []], [[], ['R', 1, 1], [], ['T', 1]], [['R', 1, 1], [], [], []], [[], [], [], []]]
    True
    >>> deplacement_thon([[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 2], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 1, 1], [], []], [['R', 1, 1], ['T', 1], ['R', 1, 1], []], [[], ['R', 1, 1], [], []], [[], [], [], []]]
    True
    '''
    grille[y][x] = gestion_gestation(grille[y][x])[:]
    grille = deplacement_vers_case_vide(grille, x, y)[:]
    return grille

def cases_thon(grille, x, y):
    '''
    Fonction qui renvoie une liste des directions dans lesquelles les cases sont
    des thons autour du point de coordonnées (x ; y).

    :param:
    grille : (list)
    x : (int) abscisse du point autour duquel on cherche les cases contenant un thon.
    y : (int) ordonnée du point autour duquel on cherche les cases contenant un thon.
    
    :renvoie:
    (list)

    >>> cases_thon([[[], ['T', 1], [], []], [['T', 1], ['R', 2, 1], ['T', 1], []], [[], ['T', 1], [], []], [[], [], [], []]], 1, 1) ==['N', 'E', 'S', 'O']
    True
    >>> cases_thon([[[], [], [], []], [['T', 1], ['R', 2, 1], ['T', 1], []], [[], ['T', 1], [], []], [[], [], [], []]], 1, 1) ==['E', 'S', 'O']
    True
    >>> cases_thon([[[], [], [], []], [['T', 1], ['R', 2, 1], [], []], [[], ['T', 1], [], []], [[], [], [], []]], 1, 1) ==['S', 'O']
    True
    >>> cases_thon([[[], [], [], []], [['T', 1], ['R', 2, 1], [], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==['O']
    True
    >>> cases_thon([[[], [], [], []], [[], ['R', 2, 1], [], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==[]
    True
    '''
    rep = []
    case = case_nord(grille, x, y)
    if case != []:
        if case[0] =='T':
            rep.append('N')
    case = case_est(grille, x, y)
    if case != []:
        if case[0] =='T':
            rep.append('E')
    case = case_sud(grille, x, y)
    if case != []:
        if case[0] =='T':
            rep.append('S')
    case = case_ouest(grille, x, y)
    if case != []:
        if case[0] =='T':
            rep.append('O')
    return rep

def deplacement_vers_thon(grille, x, y):
    '''
    Fonction qui déplace, dans la mesure du possible, de manière aléatoire, un
    requin dans une case contenant un thon.
    Elle appelle la fonction naissance si en cas de déplacement, une naissance a lieu.

    :param:
    grille : (list) Grille contenant les poissons.
    x : (int) abscisse du point à partir duquel on va aller vers une cases
    contenant éventuellement un thon.
    y : (int) ordonnée du point à partir duquel on va aller vers une cases
    contenant éventuellement un thon.

    :renvoie:
    (list)

    >>> deplacement_vers_thon([[[], ['T', 1], [], []], [[], ['R', 2, 3], [], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==[[[], ['R', 2, 3], [], []], [[], [], [], []], [[], [], [], []], [[], [], [], []]]
    True
    >>> deplacement_vers_thon([[[], [], [], []], [[], ['R', 2, 3], ['T', 1], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==[[[], [], [], []], [[], [], ['R', 2, 3], []], [[], [], [], []], [[], [], [], []]]
    True
    >>> deplacement_vers_thon([[[], [], [], []], [[], ['R', 2, 3], [], []], [[], ['T', 1], [], []], [[], [], [], []]], 1, 1) ==[[[], [], [], []], [[], [], [], []], [[], ['R', 2, 3], [], []], [[], [], [], []]]
    True
    >>> deplacement_vers_thon([[[], [], [], []], [['T', 1], ['R', 2, 3], [], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==[[[], [], [], []], [['R', 2, 3], [], [], []], [[], [], [], []], [[], [], [], []]]
    True
    >>> deplacement_vers_thon([[[], [], [], []], [[], ['R', 2, 3], [], []], [[], [], [], []], [[], [], [], []]], 1, 1) ==[[[], [], [], []], [[], ['R', 2, 3], [], []], [[], [], [], []], [[], [], [], []]]
    True
    '''
    global E_REQUIN
    cases_dispos = cases_thon(grille, x, y)
    if len(cases_dispos) !=0:
        grille[y][x][2] = E_REQUIN
        direction = cases_dispos[random.randrange(0, len(cases_dispos))]
        if direction =='N':
            grille[y - 1][x] = grille[y][x][:]
        elif direction =='E':
            grille[y][(x + 1) % len(grille[0])] = grille[y][x][:]
        elif direction =='S':
            grille[(y + 1) % len(grille)][x] = grille[y][x][:]
        else:
            grille[y][x - 1] = grille[y][x][:]
        grille = naissance(grille, x, y)[:]
    return grille

def deplacement_requin(grille, x, y):
    '''
    Fonction qui est appelée si la case choisie au hasard contient un requin.
    Elle appelle les fonctions gestion_gestation, deplacement_vers_thon et
    éventuellement deplacement_case_vide.
    
    :param:
    grille : (list) Grille contenant les poissons.
    x : (int) abscisse du point à partir duquel on va déplacer éventuellement
    un requin.
    y : (int) ordonnée du point à partir duquel on va déplacer éventuellement
    un requin.
    
    :renvoie:
    (list)
    
    :effet de bord:
    Modifie la grille donnée en paramètre.
    
    >>> deplacement_requin([[[], ['T', 2], []], [[], ['R', 4, 3], []], [[], [], []]], 1, 1) ==[[[], ['R', 3, 3], []], [[], [], []], [[], [], []]]
    True
    >>> deplacement_requin([[[], ['T', 2], []], [[], ['R', 1, 3], []], [[], [], []]], 1, 1) ==[[[], ['R', 4, 3], []], [[], ['R', 4, 3], []], [[], [], []]]
    True
    >>> deplacement_requin([[[], ['T', 2], []], [[], ['R', 2, 0], []], [[], [], []]], 1, 1) ==[[[], ['R', 1, 3], []], [[], [], []], [[], [], []]]
    True
    '''
    global E_REQUIN
    grille[y][x][2] -= 1
    grille[y][x] = gestion_gestation(grille[y][x])[:]
    grille = deplacement_vers_thon(grille, x, y)
    if grille[y][x] !=[]:
        if (grille[y][x][2] !=E_REQUIN) and (grille[y][x][2] !=0):
            grille = deplacement_vers_case_vide(grille, x, y)[:]
    if grille[y][x] !=[]:
        if grille[y][x][2] ==0:
            grille[y][x] = []
    return grille

def decompte(grille):
    '''
    Fonction qui décompte à l'instant t le nombre de thons et de requins.
    La fonction renvoie une liste de type [nb thons, nb requins].
    
    :param:
    grille : (list) Grille contenant les poissons
    
    :renvoie:
    (list)
    
    >>> decompte([[[], [], [], ['T', 2]], [['T', 2], [], [], []], [['R', 4, 3], ['T', 2], ['T', 2], ['R', 4, 3]], [['T', 2], [], [], []]]) ==[5, 2]
    True
    '''
    nb_thons, nb_requins = 0, 0
    for y in range(len(grille)):
        for x in range (len(grille[0])):
            if grille[y][x] !=[]:
                if grille[y][x][0] =='T':
                    nb_thons += 1
                else:
                    nb_requins += 1
    return [nb_thons, nb_requins]

def graphique(coordo):
    '''
    Fonction qui prend comme paramètres une liste de trois listes.
    La fonction trace deux courbes, l'une avec la première liste (abscisses) et
    la deuxième (ordonnées) et l'uatres avec encore la première (abscisse) et la
    troisième (ordonnées).
    Cette fonction utilise les modules numpy et matplotlib.
    
    :param:
    coordo : (list) Liste de trois listes contenant les coordonnées des deux
    séries de points à afficher.
    
    :renvoie:
    (NoneType)
    
    :effet de bord:
    Affiche les deux courbes dans une fenêtre.
    '''
    x = numpy.array(coordo[0])
    y1 = numpy.array(coordo[1])
    y2 = numpy.array(coordo[2])
    plt.plot(x, y1)
    plt.plot(x, y2)
    plt.title('Population des thons et des requins')
    plt.show()
    return

def routine_principale(grille):
    '''
    Fonction qui répète 10 000 fois :
    -) le choix au hasard d'une case de la grille ;
    -) le déplacement éventuel du poisson présent dans la case ;
    -) le décompte du nombre de poissons ;
    -) l'affichage des variations des effectifs des thons et requins.
    
    :param:
    grille : (list) La grille contenant les poissons.
    
    :renvoie:
    (NoneType)
    
    :effets de bord:
    -) Modifie la grille donnée en paramètre ;
    -) Affiche une fenêtre avec un graphique.
    '''
    compteur = 0
    nb_cases = len(grille) * len(grille[0])
    nb_poissons = decompte(grille)[:]
    #for _ in range(nb_cases * 100):
    while (compteur <nb_cases * 200
           ) and (
               nb_poissons[0] + nb_poissons[1] >0
                ) and (
                    nb_poissons[0] <nb_cases
                    ):
        compteur += 1
        if compteur % nb_cases ==0:
            afficher(grille)
            time.sleep(0.1)
        nb_poissons = decompte(grille)[:]
        coordonnees[0].append(compteur)
        coordonnees[1].append(nb_poissons[0])
        coordonnees[2].append(nb_poissons[1])
        x = random.randrange(0, len(grille[0]))
        y = random.randrange(0, len(grille))
        if grille[y][x] != []:
            if grille[y][x][0] =='T':
                grille = deplacement_thon(grille, x, y)[:]
            else:
                grille = deplacement_requin(grille, x, y)[:]
    graphique(coordonnees)
    return 

if __name__ == "__main__":
    import doctest
    doctest.testmod()
