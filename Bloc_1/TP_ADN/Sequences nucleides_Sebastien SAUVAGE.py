# -*- coding: utf-8 -*-

# Auteur : Sébastien SAUVAGE
# Date 9 mai 2019
# Version 1.0

import random

def estADN(chaine):
    '''
        Vérifie si la chaîne de caractères passée en paramètre ne contient aucun
        autre caractère que les bases A, C, G et T.
        
        caractere : chaine à vérifier
        caractere : type str
        
        CU : Aucune
        
        renvoie : type bool
        
        >>> estADN('ATGCGATC')
        True
        >>> estADN('ACKT')
        False
        >>> estADN('ACTK')
        False
        >>> estADN("")
        True
    '''
    rep = True
    BASES = ['A', 'C', 'G', 'T']
    for lettre in chaine:
        rep = rep and (lettre in BASES)
    return rep

def genereADN(n):
    '''
        Génère une séquence ADN aléatoirement de taille n.
        
        n : taille de la séquence
        n : type int
        
        CU : n > 0
        
        renvoie : type str
    '''
    chaineADN = ""
    BASES = ['A', 'C', 'G', 'T']
    for index in range (n):
        chaineADN += BASES[random.randrange(0, 4)]
    return chaineADN

def baseComplementaire(base, typesequence):
    '''
        Renvoie la base complémentaire de la base passée en paramètre, selon le
        type de sequence demandée.
        
        base :
        base : type str
        
        typesequence :
        typesequence : type str
        
        CU : base = 'A' ou 'T' ou 'G' ou 'C' ou 'U'
        CU : typesequence = 'ADN' ou 'ARN'
        
        renvoie : type str
        
        >>> baseComplementaire('G', 'ADN')
        'C'
        >>> baseComplementaire('A', 'ARN')
        'U'
        
    '''
    BASES = ['A', 'C', 'G', 'T']
    if typesequence =='ADN':
        BASES_REP = ['T', 'G', 'C', 'A']
    else:
        BASES_REP = ['U', 'G', 'C', 'A']
    return BASES_REP [BASES.index(base)]

def transcrit(chaine, debut, fin):
    '''
        Renvoie l'ARN construit à partir de la sous-séquence d'ADN comprise
        entre les deux positions passées en paramètre, incluses.
        
        chaine : séquence ADN
        chaine : type str
        
        debut : index du premier caractère traité
        debut : type int
        
        fin : index du dernier caractère traité
        fin : type int
        
        CU : chaine est exclusivement constitué de caractères 'A', 'T', 'G', 'C'
        CU : 0 < debut <= fin <= len(chaine) + 1
        
        renvoie : type str
        
        >>> transcrit('TTCTTCTTCGTACTTTGTGCTGGCCTCCACACGATAATCC', 4, 23)
        'AAGAAGCAUGAAACACGACC'
    '''
    rep = ""
    for caractere in chaine [debut - 1 : fin]:
        rep += baseComplementaire(caractere, 'ARN')
    return rep

def codeGenetique(codon):
    '''
        Renvoie l'acide aminé (sous la forme d'une abréviation sur une lettre)
        correspondant au codon passé en paramètre, ou * pour les codons Stop.
        
        codon : codon à coder
        codon : type chaine
        
        CU : codon est une chaine de trois caractères constituée de 'A', 'C', 'U'
        et 'G'
        
        renvoie : type chaine de un caractère
        
        >>> codeGenetique('ACU')
        'T'
        >>> codeGenetique('GUA')
        'V'
        >>> codeGenetique('AUG')
        'M'
    '''
    CODE_CODONS = {
        'UUU': 'F', 'UUC' : 'F',
        'UUA' : 'L', 'UUG' : 'L', 'CUU' : 'L', 'CUC' : 'L', 'CUA' : 'L', 'CUG' : 'L',
        'AUU' : 'I', 'AUC' : 'I', 'AUA' : 'I',
        'AUG' : 'M',
        'GUU' : 'V', 'GUC' : 'V', 'GUA' : 'V', 'GUG' : 'V',
        'UCU' : 'S', 'UCC' : 'S', 'UCA' : 'S', 'UCG' : 'S', 'AGU' : 'S', 'AGC' : 'S',
        'CCU' : 'P', 'CCC' : 'P', 'CCA' : 'P', 'CCG' : 'P',
        'ACU' : 'T', 'ACC' : 'T', 'ACA' : 'T', 'ACG' : 'T',
        'GCU' : 'A', 'GCC' : 'A', 'GCA' : 'A', 'GCG' : 'A',
        'UAU' : 'Y', 'UAC' : 'Y',
        'UAA' : '*', 'UAG' : '*', 'UGA' : '*',
        'CAU' : 'H', 'CAC' : 'H',
        'CAA' : 'Q', 'CAG' : 'Q',
        'AAU' : 'N', 'AAC' : 'N',
        'AAA' : 'K', 'AAG' : 'K',
        'GAU' : 'D', 'GAC' : 'D',
        'GAA' : 'E', 'GAG' : 'E',
        'UGU' : 'C', 'UGC' : 'C',
        'UGG' : 'W',
        'CGU' : 'R', 'CGC' : 'R', 'CGA' : 'R', 'CGG' : 'R', 'AGA' : 'R', 'AGG' : 'R',
        'GGU' : 'G', 'GGC' : 'G', 'GGA' : 'G', 'GGG' : 'G'
        }
    return CODE_CODONS[codon]

def traduit(sequenceARN):
    '''
        Construit la séquence protéique obtenue par la traduction de la séquence
        ARN passée en paramètre.
        
        sequenceARN : sequence ARN
        sequenceARN : type str
        
        CU : chaine constituée de caractères 'A', 'C', 'U' et 'G'
        CU : len(chaine) % 3 == 0
        
        renvoie : type str
        
        >>> traduit('AUGCGAAGCCGAAAGAACACCGGCUAA')
        'MRSRKNTG*'
    '''
    rep = ""
    for index in range(len(sequenceARN) // 3):
        rep += codeGenetique(sequenceARN[index * 3 : index * 3 + 3])
    return rep
     
def replique (sequenceADN):
    '''
        Construit la séquence ADN complémentaire et inversée de celle passée en paramètre.
        
        sequenceADN : séquence ADN à inverser
        sequenceADN : type str
        
        CU : sequence est une chaine contenant exclusivement des 'A', 'C', 'T' et 'G'.
        
        renvoie : type str
        
        >>> replique('ACTG')
        'CAGT'
    '''
    sequenceADNcomp = ""
    for lettre in sequenceADN:
        sequenceADNcomp += baseComplementaire(lettre, 'ADN')
    return sequenceADNcomp [::-1]

if __name__ == "__main__":
    import doctest
    doctest.testmod()
