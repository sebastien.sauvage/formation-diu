#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
:Auteur: Sébastien SAUVAGE <sebastien.sauvage@ac-lille.fr>
:Date: Juin 2019


'''

import Competitor
import Time
import tris


def read_competitors(src):
    '''
    A partir du fichier dont le nom est en paramètre, la fonction renvoie un
    dictionnaire contenant comme clé le numéro de dossard et les valeurs des
    compétiteurs associés.
    
    :param:
    src : (str) Nom du fichier CSV contenant les données des inscrits.
    
    :return:
    (dic)
    
    >>> len(read_competitors('data/small_inscrits.csv')) ==10
    True
    >>> read_competitors('data/small_inscrits.csv')[5] == {'bib_num': 5, 'first_name': 'Namo', 'last_name': 'Lereau', 'sex': 'M', 'birth_date': '26/3/1980', 'performance': None}
    True
    >>> read_competitors('small_inscrits.csv') == {}
    True
    '''
    try:
        fichier_src = open(src, "r")
        donnees = fichier_src.readlines()
        donnees.pop(0)
        # on retire le premier élément puisqu'il contient les entêtes
        dossard = 0
        inscrits_dic = {}
        for ligne in donnees:
            dossard +=1
            coureur_inscrit = ligne.rstrip('\n\r').split(';')
            inscrits_dic [dossard] = Competitor.create(coureur_inscrit[0],
                                                       coureur_inscrit[1],
                                                       coureur_inscrit[2],
                                                       coureur_inscrit[3],
                                                       dossard)
        fichier_src.close()
    except FileNotFoundError :
        inscrits_dic = {}
    return inscrits_dic

def affichage(collection):
    '''
    Affiche sur la sortie standard chacune des données de l'itérable donné en
    paramètre à raison d'une par ligne.
    
    :param:
    collection : (type iterable) Collection d'éléments de type Competitor
    
    :return:
    None
    
    :Effet de bord:
    Affiche la liste des éléments contenus dans collection.
    
    :CU:
    Les éléments du paramètre collection sont de type Competitor.
    
    >>> affichage(list(read_competitors('data/small_inscrits.csv').values()))
    [1]: Sidney Robert (M - 21/7/1970) 
    [2]: Paien Gilbert (M - 26/11/1953) 
    [3]: Vincent Riquier (M - 16/9/1980) 
    [4]: Saville Marier (M - 19/11/1969) 
    [5]: Namo Lereau (M - 26/3/1980) 
    [6]: Romaine Hughes (F - 17/10/1943) 
    [7]: Archard Rivard (M - 10/6/1950) 
    [8]: Cheney Chassé (M - 21/3/1949) 
    [9]: Avelaine CinqMars (F - 14/2/1983) 
    [10]: Sidney Charest (M - 5/3/1981) 
    '''
    for num_elmt in range(len(collection)):
        print(Competitor.to_string(collection[num_elmt]))
    return

def select_competitor_by_bib(compet_dic, dossard):
    '''
    Recherche dans le dictionnaire donné en premier paramètre le compétiteur dont
    le dossard est donné en second paramètre.
    S'il n'y a pas de coureur correspondant au critère, la fonction renvoie une
    liste vide.
    
    :param:
    compet_dic : (dic) Dictionnaire dont les valeurs sont de type Competitor.
    dossard : (int) Dossard recherché.
    
    :return:
    (?)
    >>> select_competitor_by_bib(read_competitors('data/small_inscrits.csv'), 5)
    {'bib_num': 5, 'first_name': 'Namo', 'last_name': 'Lereau', 'sex': 'M', 'birth_date': '26/3/1980', 'performance': None}
    '''
    try :
        compet = compet_dic[dossard]
    except :
        compet = None
    return compet

def select_competitor_by_birth_year(compet_dic, annee):
    '''
    Recherche dans le dictionnaire donné en premier paramètre les compétiteurs dont
    l'année de naissance est donnée en second paramètre.
    S'il n'y a pas de coureur correspondant au critère, la fonction renvoie une
    liste vide.
    
    :param:
    compet_dic : (dic) Dictionnaire dont les valeurs sont de type Competitor.
    annee : (str) Année de naissance recherchée au format str.
    
    :return:
    (list)
    
    >>> len(select_competitor_by_birth_year(read_competitors('data/small_inscrits.csv'), '1980')) == 2
    True
    >>> len(select_competitor_by_birth_year(read_competitors('data/small_inscrits.csv'), '1981')) == 1
    True
    >>> len(select_competitor_by_birth_year(read_competitors('data/small_inscrits.csv'), '1999')) == 0
    True
    '''
    compet_lst = []
    for coureur in compet_dic.values():
        if Competitor.get_birthdate(coureur).endswith(annee, -4):
            compet_lst.append(coureur)
    return compet_lst

def select_competitor_by_name(compet_dic, nom):
    '''
    Donne la liste des compétiteurs dont le nom contient la chaîne de caractère
    passée en paramètre.
    
    :param:
    compet_dic : (dic) Dictionnaire des compétiteurs.
    nom : (str) Nom des coureurs à conserver dans le dictionnaire donné en paramètre.
    
    :return:
    (list)
    
    >>> len(select_competitor_by_name(read_competitors('data/small_inscrits.csv'), 'Sidney')) ==2
    True
    '''
    compet_lst = []
    for coureur in compet_dic.values():
        if Competitor.get_firstname(coureur) ==nom:
            compet_lst.append(coureur)
    return compet_lst

def read_performances(src):
    '''
    Renvoie un dictionnaire des performances contenues dans le fichier donné en
    paramètre.
    
    :param:
    src : (str) Fichier source contenant les les données de performance.
    
    :return:
    (dic)
    
    >>> len(read_performances('data/small_performances.csv')) ==8
    True
    >>> read_performances('data/small_performances.csv')[5]
    Time(hours=1, minutes=6, seconds=20)
    >>> read_performances('small_performances.csv') == {}
    True
    '''
    try:
        fichier_src = open(src, "r")
        donnees = fichier_src.readlines()
        donnees.pop(0)
        temps_dic = {}
        for ligne in donnees:
            coureur_donnee = ligne.rstrip('\n\r').split(';')
            temps_dic [int(coureur_donnee[0])] = Time.create(int(coureur_donnee[1]),
                                                            int(coureur_donnee[2]),
                                                            int(coureur_donnee[3]))
        fichier_src.close()
    except FileNotFoundError :
        temps_dic = {}
    return temps_dic

def set_performances(compet_dic, temps_dic):
    '''
    Modifie le dictionnaire donné en premier paramètre en y ajoutant les temps
    contenus dans le second paramètre. La correspondance s'effectue par les clés
    des dictionnaires donnés en paramètres.
    
    :param:
    compet_dic : (dic) Données des coureurs inscrits
    temps_dic : (dic) Temps des coureurs arrivés
    
    :return:
    (dic)
    
    >>> len(set_performances(read_competitors('data/small_inscrits.csv'),read_performances('data/small_performances.csv'))) ==10
    True
    >>> set_performances(read_competitors('data/small_inscrits.csv'),read_performances('data/small_performances.csv'))[2]['performance'] ==None
    True
    >>> set_performances(read_competitors('data/small_inscrits.csv'),read_performances('data/small_performances.csv'))[10]['performance']
    Time(hours=1, minutes=6, seconds=38)
    '''
    for cle, valeur in temps_dic.items():
        Competitor.set_performance(compet_dic[cle], valeur)
    return compet_dic

def sort_competitors_by_lastname(compet_dic):
    '''
    Trie les valeurs contenues dans le paramètre selon l'ordre alphabétique des
    noms des participants.
    
    :param:
    compet_dic : (dic) Données d'un compétiteur.
    
    :return:
    (list)
    
    >>> sort_competitors_by_lastname(read_competitors('data/small_inscrits.csv'))[0]
    {'bib_num': 10, 'first_name': 'Sidney', 'last_name': 'Charest', 'sex': 'M', 'birth_date': '5/3/1981', 'performance': None}
    '''
    compet_list = list(compet_dic.values())
    return tris.tri_insertion(compet_list, Competitor.compare_lastname)

def sort_competitors_by_performance(compet_dic):
    '''
    Trie les valeurs contenues dans le paramètre selon la performance des
    participants.
    
    :param:
    compet_dic : (dic) Données d'un compétiteur.
    
    :return:
    (list)
    
    sort_competitors_by_performance(set_performances(read_competitors('data/small_inscrits.csv'),read_performances('data/small_performances.csv')))
    
    '''
    compet_list = list(compet_dic.values())
    return tris.tri_insertion(compet_list, Competitor.compare_performance)

def print_results_alpha(compet_dic):
    '''
    Imprime sur la sortie standard le prénom, nom, sexe, numéro de dossard et
    performance des coureurs contenus dans le paramètre. Les coureurs sont triés
    par ordre alphabétique.
    
    :param:
    compet_dic : (dic) Dictionnaire dont les valeurs sont les coureurs à afficher.
    
    :return:
    None
    
    :Effet de bord:
    Imprime dans la sortie standard.
    
    :Exemples:
    Pour afficher la liste des coureurs triés par nom :
    print_results_alpha(set_performances(read_competitors('data/small_inscrits.csv'),read_performances('data/small_performances.csv')))
    '''
    compet_tries_liste = sort_competitors_by_lastname(compet_dic)
    for coureur in compet_tries_liste:
        coureur_str = Competitor.to_string(coureur)
        try:
            tps_str = Time.to_string(Competitor.get_performance(coureur))
        except:
            tps_str = ''
        print('{:40} => {:10}'.format(coureur_str, tps_str))
    return

def print_results_performance(compet_dic):
    '''
    Imprime sur la sortie standard le prénom, nom, sexe, numéro de dossard et
    performance des coureurs contenus dans le paramètre. Les coureurs sont triés
    par ordre des performances.
    
    :param:
    compet_dic : (dic) Dictionnaire dont les valeurs sont les coureurs à afficher.
    
    :return:
    None
    
    :Effet de bord:
    Imprime dans la sortie standard.
    
    :Exemples:
    Pour afficher la liste des coureurs triés par performance :
    print_results_performance(set_performances(read_competitors('data/small_inscrits.csv'),read_performances('data/small_performances.csv')))
    '''
    compet_tries_liste = sort_competitors_by_performance(compet_dic)
    for coureur in compet_tries_liste:
        coureur_str = Competitor.to_string(coureur)
        try:
            tps_str = Time.to_string(Competitor.get_performance(coureur))
        except:
            tps_str = ''
        print('{:40} => {:10}'.format(coureur_str, tps_str))
    return

def save_results(compet_dic, dest):
    '''
    Sauvegarde dans le fichier destination, donné en second paramètre, le numéro
    le dossard, le prénom, le nom et la performance des coureurs contenus dans
    le dictionnaire.
    
    :param:
    compet_dic : (dic) Contient l'ensemble des coureurs.
    dest : (str) Nom du fichier dans lequel il faut sauvergader les données.
    
    :return:
    None
    
    :Effet de bord:
    Imprime dans le fichier dest, donné en paramètre.
    
    d = sort_competitors_by_performance(set_performances(read_competitors('data/small_inscrits.csv'),read_performances('data/small_performances.csv')))
    save_results(d, 'resultats.csv')
    
    save_results(set_performances(read_competitors('data/small_inscrits.csv'),read_performances('data/small_performances.csv')), 'resultats.csv')
    '''
    compet_tries_liste = sort_competitors_by_performance(compet_dic)
    with open(dest, "w", encoding = 'utf-8') as fichier_dest:
        fichier_dest.write('Num_dossard;Prénom;Nom;Performance\n')
        for coureur in compet_tries_liste:
            try:
                nvl_ligne = '{};{};{};{}\n'.format(Competitor.get_bib_num(coureur),
                                             Competitor.get_firstname(coureur),
                                             Competitor.get_lastname(coureur),
                                             Time.to_string(Competitor.get_performance(coureur)))
            except AttributeError:
                nvl_ligne = '{};{};{};{}\n'.format(Competitor.get_bib_num(coureur),
                                             Competitor.get_firstname(coureur),
                                             Competitor.get_lastname(coureur),
                                             '')
            fichier_dest.write(nvl_ligne)

def est_homme(coureur):
    '''
    Prédicat qui détermine si le coureur, passé en paramètre, est un homme.
    
    :param:
    coureur : (Competitor) Données d'un coureur.
    
    :return:
    (bool)
    '''
    return Competitor.get_sex(coureur) =='M'

def est_femme(coureur):
    '''
    Prédicat qui détermine si le coureur, passé en paramètre, est une femme.
    
    :param:
    coureur : (Competitor) Données d'un coureur.
    
    :return:
    (bool)
    '''
    return not est_homme(coureur)

def select_competitor(compet_dic, predicat):
    '''
    Fonction qui renvoie la liste des compétiteurs qui vérifient le prédicat.
    
    :param:
    compet_dic : (dic) Dictionnaire de compétiteurs.
    prédicat : (str) Nom du prédicat qui permet de sélectionner.
    
    :return:
    (list)
    
    :exemple:
    select_competitor(read_competitors('data/small_inscrits.csv'), est_femme)
    '''
    coureurs_retenus = []
    for coureur in compet_dic.values():
        if predicat(coureur):
            coureurs_retenus.append(coureur)
    return coureurs_retenus

def est_ne_en(coureur, annee):
    '''
    Prédicat qui détermine si le coureur, passé en paramètre, est né dans l'année
    indiquée en second paramètre.
    
    :param:
    coureur : (Competitor) Données d'un coureur.
    annee : (str) Année de naissance.
    
    :return:
    (bool)
    '''
    return not Competitor.get_birthdate(coureur).endswith(annee, -4)

def select_competitor_by_birth_day2(ccompet_dic, annee):
    '''
    Recherche dans le dictionnaire donné en premier paramètre les compétiteurs dont
    l'année de naissance est donnée en second paramètre.
    S'il n'y a pas de coureur correspondant au critère, la fonction renvoie une
    liste vide.
    
    :param:
    compet_dic : (dic) Dictionnaire dont les valeurs sont de type Competitor.
    annee : (str) Année de naissance recherchée au format str.
    
    :return:
    (list)
    
    >>> len(select_competitor_by_birth_year(read_competitors('data/small_inscrits.csv'), '1980')) == 2
    True
    >>> len(select_competitor_by_birth_year(read_competitors('data/small_inscrits.csv'), '1981')) == 1
    True
    >>> len(select_competitor_by_birth_year(read_competitors('data/small_inscrits.csv'), '1999')) == 0
    True
    '''
    compet_lst = []
    for coureur in compet_dic.values():
        if est_ne_en(coureur, annee):
            compet_lst.append(coureur)
    return compet_lst

def s_appelle(coureur, nom):
    '''
    Prédicat qui détermine si le coureur, passé en paramètre, s'appelle selon le
    nom indiqué en second paramètre.
    
    :param:
    coureur : (Competitor) Données d'un coureur.
    annee : (str) Nom recherché.
    
    :return:
    (bool)
    '''
    return Competitor.get_firstname(coureur) ==nom

def select_competitor_by_name2(compet_dic, nom):
    '''
    Donne la liste des compétiteurs dont le nom contient la chaîne de caractère
    passée en paramètre.
    
    :param:
    compet_dic : (dic) Dictionnaire des compétiteurs.
    nom : (str) Nom des coureurs à conserver dans le dictionnaire donné en paramètre.
    
    :return:
    (list)
    
    >>> len(select_competitor_by_name(read_competitors('data/small_inscrits.csv'), 'Sidney')) ==2
    True
    '''
    compet_lst = []
    for coureur in compet_dic.values():
        if s_appelle(coureur, nom):
            compet_lst.append(coureur)
    return compet_lst



if __name__ == "__main__":
    import doctest
    doctest.testmod()
    l=set_performances(read_competitors('data/small_inscrits.csv'),read_performances('data/small_performances.csv'))
    