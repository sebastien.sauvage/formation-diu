#!/usr/bin/env python
# -*- coding: utf-8 -*-

def recopie(src, dest):
    '''
    Lit chaque ligne contenue dans le fichier src et les recopie dans le second
    après avoir remplacé les o par des 0.
    
    :param:
    src : (str) Nom d'un fichier texte en entrée
    dest : (str) Nom d'un fichier texte en sortie
    
    :renvoie:
    (None)
    
    :effet de bord:
    Crée et modifie le contenu du fichier dest s'il n'existe pas
    '''
    fichier_src = open(src, "r")
    fichier_dest = open(dest, "w")
    for ligne in fichier_src:
        nvl_ligne = ligne.replace("o", "0")
        fichier_dest.write(nvl_ligne)
    fichier_dest.close()
    fichier_src.close()
    return

def recopie2(src, dest):
    '''
    Lit chaque ligne contenue dans le fichier src et les recopie dans le second
    après avoir remplacé les o par des 0.
    Cette fonction effectue les mêmes tâches que recopie(src,dest) mais utilise
    with afin d'éviter l'usage de la méthode .close().
    
    :param:
    src : (str) Nom d'un fichier texte en entrée
    dest : (str) Nom d'un fichier texte en sortie
    
    :renvoie:
    (None)
    
    :effet de bord:
    Crée et modifie le contenu du fichier dest s'il n'existe pas
    '''
    with open(src, "r", encoding = 'utf-8') as fichier_src:
        with open(dest, "w", encoding = 'utf-8') as fichier_dest:
            for ligne in fichier_src:
                nvl_ligne = ligne.replace("o", "0")
                fichier_dest.write(nvl_ligne)
    return