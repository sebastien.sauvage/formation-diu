#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
:Auteur: Sébastien SAUVAGE <sebastien.sauvage@ac-lille.fr>
:Date: Juin 2019

Le module Time.py définit un type Time qui correspond à un tuple nommé possédant
trois champs : hours, minutes, seconds.

Ce module contient trois fonctions : create, compare et to_string
Se référer à la documentation de ces fonctions pour plus d'information.
'''

import collections

def create(h, m, s):
    '''
    Permet de créer une donnée de type "Time", c'est à dire un tuple nommé à trois
    champs : hours, minutes et seconds
    
    :param:
    h : (int) nombres d'heures du temps de courses.
    m : (int) nombre de minutes du temps de course.
    s : (int) nombre de secondes du temps de course.
    
    :return:
    (?)
    
    >>> create(1,10,2)
    Time(hours=1, minutes=10, seconds=2)
    >>> create(-1,10,2) ==None
    True
    >>> create(1,70,2) ==None
    True
    >>> create(1.3,5,6) ==None
    True
    '''
    if (
        type(h) ==int and type(m) ==int and type(s) ==int
        ) and (
        h >=0 and m >=0 and m <60 and s >=0 and s <60
        ):
        Time = collections.namedtuple('Time', ['hours', 'minutes', 'seconds'])
        tps = Time(h, m, s)
    else:
        tps = None
    return tps

def compare(Time1, Time2):
    '''
    Définit une relation d'ordre sur les données de type Time. De manière classique,
    le résultat est négatif si Time1 < Time2, positif si Time1 > Time 2 et 0 en
    cas d'égalité.
    
    :param:
    Time1 : (?) Premier temps à comparer
    Time2 : (?) Second temps à comparer
    
    :return:
    (int)
    
    >>> compare(create(0,58,3), create(1,2,45)) < 0
    True
    >>> compare(create(1,1,3), create(1,2,45)) < 0
    True
    >>> compare(create(1,2,3), create(1,2,45)) < 0
    True
    >>> compare(create(1,2,45), create(1,2,3)) > 0
    True
    >>> compare(create(1,2,3), create(1,2,3)) == 0
    True
    >>> compare(None, create(1,2,3)) > 0
    True
    >>> compare(create(1,2,3), None) < 0
    True
    >>> compare (None, None) < 0
    True
    '''
    try:
        Tps_en_sec2 = Time2.hours * 3600 + Time2.minutes * 60 + Time2.seconds 
        try:
            Tps_en_sec1 = Time1.hours * 3600 + Time1.minutes * 60 + Time1.seconds
            rep = Tps_en_sec1 - Tps_en_sec2
        except:
            # Cas où seul le premier compétiteur n'a pas de temps.
            rep = 1
    except:
        rep = -1
            
    return rep

def to_string(Time):
    '''
    Renvoie une représentation du paramètre sous la forme d'une chaîne de caractères
    
    :param:
    Time : (?) Temps à convertir en chaîne de caractère.
    
    :return:
    (str)
    
    >>> to_string(create(1,2,3)) == '1:02:03'
    True
    >>> to_string(create(0,58,3)) == '0:58:03'
    True
    '''
    return "{0:1}:{1:02}:{2:02}".format(Time.hours, Time.minutes, Time.seconds)

if __name__ == "__main__":
    import doctest
    doctest.testmod()