# -*- coding: utf-8 -*-

# Auteur : Sébastien SAUVAGE
# Date 9 mai 2019
# Version 1.0

import random
import time

def creer_grille(larg, haut):
    '''
        Renvoie une liste de listes correspondant à une grille du jeu de la vie
        aux dimensions souhaitées, ne contenant aucune cellule.
        
        larg : nombre de colonne de la grille
        larg : type int
        
        haut : nombre de lignes de la grille
        haut : type int
        
        CU : larg, haut > 0
        
        renvoie : list of list
        
        >>> creer_grille(3, 2)
        [[0, 0, 0], [0, 0, 0]]
    '''
    ligne = [0] * larg
    grille =[]
    for l in range(haut):
        grille.append(ligne)
    return grille

def hauteur_grille(grille):
    '''
        Prend en paramètre une grille de jeu de la vie et renvoie le nombre de
        cases verticalement.
        
        grille : grille du jeu de la vie
        grille : list of list
        
        CU : Les listes sont constituées de 0 et de 1.
        
        renvoie : int
        
        >>> hauteur_grille(creer_grille(3, 2))
        2
    '''
    return len(grille)

def largeur_grille(grille):
    '''
        Prend en paramètre une grille de jeu de la vie et renvoie le nombre de
        cases horizontalement.
        
        grille : grille du jeu de la vie
        grille : list of list
        
        CU : Les listes sont constituées de 0 et de 1.
        
        renvoie : int
        
        >>> largeur_grille(creer_grille(3, 2))
        3
    '''
    return len(grille[0])

def creer_grille_aleatoire(larg, haut, p):
    '''
        larg : nombre de colonnes de la grille
        larg : int
        
        haut : nombre de lignes de la grille
        haut : int
        
        p : probabilité d'avoir une cellule dans une case
        p : float
        
        CU : larg, haut > 0
        CU : 0<= p <=1
        
        renvoie : list of list
        
        >>> creer_grille_aleatoire(3, 2, 1)
        [[1, 1, 1], [1, 1, 1]]
        >>> creer_grille_aleatoire(3, 2, 0)
        [[0, 0, 0], [0, 0, 0]]
    '''
    grille = []
    for l in range(haut):
        ligne = []
        for c in range(larg):
            if random.random() < p:
                cellule = 1
            else:
                cellule = 0
            ligne.append(cellule)
        grille.append(ligne)
    return grille

def voisins_case(grille, x, y):
    '''
        Renvoie une liste contenant la valeur des cases voisines de la case donnée.
        
        grille : grille du jeude la vie
        grille : type list
        
        x : abscisse de la case donnée.
        x : type int
        
        y : ordonnée de la case donnée.
        y : type int
        
        CU : 0 <= y <= len(grille)
        CU : 0 <= x <= len(grille[0])
        
        renvoie : type list
        
        >>> grille = [[0, 1, 0], [1, 0, 0], [1, 1, 1]]
        >>> print(voisins_case(grille, 1, 1))
        [0, 1, 0, 1, 0, 1, 1, 1]
        >>> grille = [[0, 1, 0], [1, 0, 0], [1, 1, 1]]
        >>> print(voisins_case(grille, 2, 2))
        [0, 0, 1]
        >>> grille = [[0, 1, 0], [1, 0, 0], [1, 1, 1]]
        >>> print(voisins_case(grille, 0, 2))
        [1, 0, 1]
    '''
    larg = largeur_grille(grille)
    haut = hauteur_grille(grille)
    contour = []
    for ligne in range(y - 1, y + 2):
        if (ligne >=0) and (ligne <haut):
            for colonne in range(x - 1, x + 2):
                if (colonne >=0) and (colonne <larg):
                    if (colonne !=x) or (ligne !=y):
                        contour.append(grille[ligne][colonne])
    return contour

def nb_cellules_voisins(grille, x, y):
    '''
        Renvoie le nombre de cellules dans les cases voisines de la case passée en
        paramètre.
    
        grille : grille du jeude la vie
        grille : type list
        
        x : abscisse de la case donnée.
        x : type int
        
        y : ordonnée de la case donnée.
        y : type int
        
        CU : 0 <= y <= len(grille)
        CU : 0 <= x <= len(grille[0])
            
        renvoie : type int
            
        >>> grille = [[0, 1, 0], [1, 0, 0], [1, 1, 1]]
        >>> print(nb_cellules_voisins(grille, 1, 1))
        5
        >>> grille = [[0, 1, 0], [1, 0, 0], [1, 1, 1]]
        >>> print(nb_cellules_voisins(grille, 2, 2))
        1
        >>> grille = [[0, 1, 0], [1, 0, 0], [1, 1, 1]]
        >>> print(nb_cellules_voisins(grille, 0, 2))
        2
    '''
    return sum(voisins_case(grille, x, y))

def afficher_grille(grille):
    '''
        Affiche de manière plus claire une grille du jeu de la vie qui lui est passée
        en paramètre.
    
        grille : grille du jeude la vie
        grille : type list
        
        renvoie : rien

    
        >>> grille = [[0, 1, 0], [1, 0, 0], [1, 1, 1]]
        >>> afficher_grille(grille)
        _ O _
        O _ _
        O O O
        >>> afficher_grille(creer_grille(3, 2))
        _ _ _
        _ _ _
    '''
    for ligne_liste in grille:
        for numColonne in range(len(ligne_liste)):
            if ligne_liste[numColonne]:
                caract = 'O'
            else:
                caract = '_'
            if numColonne <len(ligne_liste) - 1:
                print (caract, end = ' ')
            else:
                print (caract)

def generation_suivante(grille):
    '''
        A partir d'une grille passée en paramètre, calcule la grille de la
        génération suivante et la retourne.La nouvelle génération est calculée à
        partir des critères d'émergence ou de mort des cellules indiqués au début
        de l'énoncé. Dans le jeu de la vie, on considère que la nouvelle génération
        apparaît spontanément dans toutes les cellules au même moment.
    
        grille : grille du jeu de la vie
        grille : type list
        
        renvoie : type list
    
        >>> grille = [[0, 1, 0], [1, 0, 0], [1, 1, 1]]
        >>> generation_suivante(grille)
        [[0, 0, 0], [1, 0, 1], [1, 1, 0]]
        >>> generation_suivante([[0, 0, 0], [1, 0, 1], [1, 1, 0]])
        [[0, 0, 0], [1, 0, 0], [1, 1, 0]]
        >>> generation_suivante([[0, 0, 0], [1, 0, 0], [1, 1, 0]])
        [[0, 0, 0], [1, 1, 0], [1, 1, 0]]
    '''
    nouvGrille = []
    for ligne in range(len(grille)):
        nouvLigne = []
        for colonne in range(len(grille[ligne])):
            if grille[ligne][colonne] ==0:
                if nb_cellules_voisins(grille, colonne, ligne) ==3:
                    res = 1
                else:
                    res = 0
            elif grille[ligne][colonne] ==1:
                if nb_cellules_voisins(grille, colonne, ligne) in [2, 3]:
                    res = 1
                else:
                    res = 0
            nouvLigne.append(res)
        nouvGrille.append(nouvLigne)
    return nouvGrille

def evolution_n_generations(grille, n):
    '''
        Affiche l'évolution de la grille au fil de n générations.  
    
        grille : grille du jeu de la vie
        grille : type list
        
        n : nombre de générations à calculer
        n : type int
        
        renvoie : type list
    '''
    for i in range(n):
        grille = generation_suivante(grille)
        afficher_grille(grille)
        #time.sleep(0.1)
        

grille_2etats = [[0, 0, 1, 0], [1, 0, 0, 1], [1, 0, 0, 1], [0, 1, 0, 0]]
grille_planneur = [[0, 1, 0, 0, 0], [0, 0, 1, 0, 0], [1, 1, 1, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]
grille_cannonplanneur = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]
 
grille = grille_cannonplanneur
evolution_n_generations(grille, 100)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
