#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Auteur : Sébastien SAUVAGE
# Date 15 mai 2019
# Version 1.0

from liste_7776_mots import LISTE_MOTS
import random

def est_tuple(tup):
    '''
    Vérifie :
    - si la paramètre tup est de type tuple
   
    :param:
    tup : (tuple) contient des mots
    
    :renvoie: True si les vérifications sont correctes
    
    >>> est_tuple(LISTE_MOTS) ==True
    True
    '''
    return isinstance(tup, tuple)

def est_long(tup, l):
    '''
    Vérifie :
    - si la longueur est bien l
    
    :param:
    tup : (tuple) contient des mots
    l : (int) doit correspondre à la longueur de tup
    
    :renvoie: True si la vérification est correcte
    
    >>> est_long(LISTE_MOTS, 7776) ==True
    True
    '''
    return len(tup) ==l

def contient_chaines(tup = LISTE_MOTS):
    '''
    Vérifie :
    - si le tuple ne contient que des chaînes
    
    :param:
    tup : (tuple) contient des mots
    
    :renvoie: True si la vérification est correcte
    
    >>> contient_chaines(LISTE_MOTS) ==True
    True
    '''
    return all(isinstance(elmt, str) for elmt in tup)
    
def tous_diff(tup = LISTE_MOTS):
    '''
    Vérifie :
    - si tous les éléments du tuple sont différents
    
    :param:
    tup : (tuple) contient des mots
    
    :renvoie: True si les vérifications sont correctes
    
    >>> tous_diff(LISTE_MOTS) ==True
    True
    '''
    verif_diff = True
    for elmt in tup:
        if tup.count(elmt) >1:
            verif_diff = False
    return verif_diff    

def verifications(tup = LISTE_MOTS):
    '''
    Vérifie :
    - si la paramètre tup est de type tuple
    - si la longueur est bien de 7 776 mots
    - si tous les éléments de tup sont des chaines de caractères
    - si tous les mots sont différents
    
    :param:
    tup : (tuple) contient des mots
    
    :renvoie: True si les vérifications sont correctes
    
    >>> verifications(LISTE_MOTS) ==True
    True
    
    '''
    return (est_tuple(tup) and
            est_long(tup, 7776) and
            contient_chaines(tup) and
            tous_diff(tup))

def long_mini_maxi(tup):
    '''
    Détermine les longueurs minimales et maximales des éléments du tuple donné
    en paramètre
    
    :param:
    tup : (tuple) contient des chaînes de caractères
    
    :CU: les éléments sont de longueur maximum 1000
    
    :renvoie:
    mini : (int) longueur minimum des éléments de tup
    maxi : (int) longueur maximum des éléments de tup
    
    >>> long_mini_maxi(LISTE_MOTS) ==(4, 9)
    True
    '''
    mini, maxi = 1000, 0
    for elmt in tup:
        mini = min(mini, len(elmt))
        maxi = max(maxi, len(elmt))
    return mini, maxi

def affiche_deb_fin_2094(tup):
    '''
    Imprime le premier mot de cette liste, le dernier, et celui d'indice 2 094
    
    :param:
    tup : (int) contient des chaines de caractères
    
    :Effets de bord:
    Imprime trois lignes
    
    :return:
    noneType
    
    '>>> affiche_deb_fin_2094(LISTE_MOTS) =='0 : montes
    7775 : tour
    2094 : morigener'
    True
    '''
    for i in [0, len(tup) - 1, 2094]:
        print(f'{i} : {tup[i]}')
    return

def en_nombre(seq_de):
    '''
    Converti une chaîne de séquence de dés en écriture décimale
    
    :param:
    seq_de : (str) chaine constituée de chiffres entre 1 et 6
    
    :renvoie:
    (int) l'écriture décimal correspondant à la séquence de dés
    
    >>> en_nombre('11111') ==0
    True
    >>> en_nombre('66666') ==7775
    True
    >>> en_nombre('24521') ==2094
    True
    '''
    nb_dec = 0
    for indice in range(len(seq_de)):
        c_indice = int(seq_de[indice])
        nb_dec += (c_indice - 1) * 6 ** (len(seq_de) - 1 - indice)
    return nb_dec

def donne_mot(seq_de, tup = LISTE_MOTS):
    '''
    Renvoie le mot de tup correspondant à la séquence de dés donnée en paramètre
    
    :param:
    seq_de : (str) chaîne constituée de de chiffres entre 1 et 6
    tup : (tuple) tuple contenant des mots
    
    :renvoie:
    (str) mot correspondant
    
    >>> donne_mot('11111') =='montes'
    True
    >>> donne_mot('66666') =='tour'
    True
    >>> donne_mot('24521') =='morigener'
    True
    '''
    return tup[en_nombre(seq_de)]

def genere_sequence_alea():
    '''
    Renvoie une chaîne de caractères constituée d'une séquences de 5 lancers de dés.
    
    :param:
    NoneType
    
    :renvoie:
    (str) une séquence de 5 lancers
    
    '>>> genere_n_sequences_alea() =='14166'
    True
    '>>> genere_n_sequence_alea() =='24521'
    True
    '''
    seq = ''
    for _ in range(5):
        seq += random.choice(['1', '2', '3', '4', '5', '6'])
    return seq
    
def genere_n_sequence_alea(n):
    '''
    Renvoie une chaîne de caractères constituée de n séquences de 5 lancers de dés.
    
    :param:
    n : (int) nombre de séquences à effectuer
    
    :renvoie:
    (str) les n séquences de 5 lancers
    
    #>>> genere_n_sequences_alea(2) =='5311514166'
    True
    #>>> genere_n_sequence_alea(3) =='111116666624521'
    True
    '''
    seq = ''
    for _ in range (n):
        seq += genere_sequence_alea()
    return seq

def genere_phrase_passe(seq):
    '''
    Renvoie une phrase de passe dont le nombre de mots la constituant dépend de
    la longueur de la séquence.
    
    :param:
    seq : (str) contient une suite de lancers de dés
    
    :renvoie:
    (str)
    
    :CU:
    La chaîne entrée en paramètres doit avoir une longueur multiple de 5.
    
    >>> genere_phrase_passe('111116666624521') =='montes-tour-morigener'
    True
    '''
    phrase_passe = ''
    for num_mot in range (len(seq) // 5):
        phrase_passe += donne_mot(seq[num_mot * 5 : num_mot * 5 + 5])
        phrase_passe +="-"
    phrase_passe = phrase_passe[:-1]
    return phrase_passe

def perturbe_chaine(s):
    '''
    Fonction qui met en majuscule de manière aléatoire certains caractères de la
    chaîne donnée en paramètre s.
    
    :param:
    s : (str) Mot dont il faut changer certaines lettres de manière aléatoire.
    
    :renvoie:
    (str)
    
    #>>> perturbe_chaine("timoleon")
    'timolEOn'
    #>>> perturbe_chaine("timoleon")
    'tIMOlEON'
    '''
    s_modif = ''
    for lettre in s:
        if random.randint(0, 1):
            s_modif += chr(ord(lettre) - 32)
        else:
            s_modif += lettre
    return s_modif

def genere_phrase_passe2(seq):
    '''
    Variante de la fonction genere_phrase_passe mais cette fois les caractères des
    lettres sont en minuscules ou majuscules aléatoirement.
    
    :param:
    seq : (str) contient une suite de lancers de dés
    
    :renvoie:
    (str)
    
    :CU:
    La chaîne entrée en paramètres doit avoir une longueur multiple de 5.
    #>>> genere_phrase_passe2('111116666624521')
    'MoNtEs-toUr-MOrIGENER'
    '''
    phrase_passe = ''
    for num_mot in range (len(seq) // 5):
        phrase_passe += perturbe_chaine(
            donne_mot(seq[num_mot * 5 : num_mot * 5 + 5])
            )
        phrase_passe +="-"
    phrase_passe = phrase_passe[:-1]
    return phrase_passe
    
def perturbe_chaine2(s):
    '''
    Variante de perturbe_chaine mais cette fois, les lettres non modifiées sont
    aussi remplacée ou non aléatoirement par une valeurs correspondante.
    
    :param:
    s : (str) Mot dont il faut changer certaines lettres de manière aléatoire.
    
    :renvoie:
    (str)
    
    #>>> perturbe_chaine2("timoleon")
    'TiMol30N'
    #>>> perturbe_chaine2("timoleon")
    'tiMol3on'
    #>>> perturbe_chaine2("timoleon")
    'T1m0LeON'
    '''
    s_modif = ''
    EQUIVALENTS = {
        'a' : '@',
        'b' : '8',
        'e' : '3',
        'i' : '1',
        'o' : '0'
        }
    for lettre in s:
        if random.randint(0, 1):
            s_modif += chr(ord(lettre) - 32)
        elif lettre in EQUIVALENTS:
            if random.randint(0, 1):
                s_modif += EQUIVALENTS[lettre]
            else:
                s_modif += lettre
        else:
            s_modif += lettre
    return s_modif

def genere_phrase_passe3(seq):
    '''
    Variante de la fonction genere_phrase_passe mais cette fois les caractères des
    lettres sont en minuscules ou majuscules aléatoirement et certains caratères
    remplacés par des valeurs associées.
    
    :param:
    seq : (str) contient une suite de lancers de dés
    
    :renvoie:
    (str)
    
    :CU:
    La chaîne entrée en paramètres doit avoir une longueur multiple de 5.
    
    #>>> genere_phrase_passe3('111116666624521')
    'm0NTes-toUr-Morig3neR'
    #>>> genere_phrase_passe3('111116666624521')
    'MonTeS-touR-mor1GENeR'
    '''
    phrase_passe = ''
    for num_mot in range (len(seq) // 5):
        phrase_passe += perturbe_chaine2(
            donne_mot(seq[num_mot * 5 : num_mot * 5 + 5])
            )
        phrase_passe +="-"
    phrase_passe = phrase_passe[:-1]
    return phrase_passe

if __name__ == "__main__":
    import doctest
    doctest.testmod()